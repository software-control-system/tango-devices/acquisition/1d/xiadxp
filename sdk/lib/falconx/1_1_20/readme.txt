http://support.xia.com/default.asp?W689#cpp2015
Handel-SiToro v1.1.20 (2018-11-13)
ProSpect-1.1.27-FalconXN-setup.exe

NB:
Visual C++ Redistributable for Visual Studio 2015
Versions of ProSpect-FalconXN later than 1.0.31 require Visual C++ Redistributable for Visual Studio 2015. 
This prerequisite may be installed on your system by Windows Update. 
If you run ProSpect and see an error popup indicating ProSpect is unable to load handel.dll, you will need to install the package directly from the Microsoft Download Center. 
Click Download, check the box for vc_redist.x86.exe, and click Next. 
Install the package and then restart ProSpect.

