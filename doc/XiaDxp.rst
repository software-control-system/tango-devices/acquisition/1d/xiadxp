.. XiaDxp:

XiaDxp
--------
.. image:: XiaDxp.png

Introduction
``````````````
The XIA Digital X-ray Processor is a family of digital spectrometers using acquisition boards XMap/FalconX

Properties
````````````````````

====================================== ==================================================================== ==================== ===============================================
Property name                          Default value                                                        Type                 Description
====================================== ==================================================================== ==================== ===============================================
AutoLoad                               False                                                                Boolean              Allow to Reload the last used configuration file (*.ini*) at each init of the device.
BoardType                              XMAP                                                                 String               Define the board type : 
                                                                                                                                    XMAP [;path of the library handel.dll]
                                                                                                                                    FALCONX [;path of the library handel.dll]
                                                                                                                                    SIMULATOR [;nbModules] [;nbChannels] [;acquisitionClockMs] [;loadConfigurationFileDelayMs]
BoardTimeBase                          0.000000320                                                          Double               Timebase factor of the board. [in seconds]
ConfigurationFiles                     ALIAS;MODE;FILE_PATH_NAME                                            Array of string      Define the list of Configuration "*.INI*" files and their associated alias & mode.
RoisFiles                              ALIAS;FILE_PATH_NAME                                                 Array of string      Define the list of rois files "*.txt*" and their associated alias.
StreamItems                            Triggers, Outputs, Icr, Ocr, RealTime, LiveTime, DeadTime, Channel   Array of string      Define the list of Items managed by the Streamer. (Nexus, CSV, ...). Each item defined here wil be saved in the stream for all available channels. Availables values are (not sensitive case):
                                                                                                                                    Triggers
                                                                                                                                    Outputs
                                                                                                                                    Icr
                                                                                                                                    Ocr
                                                                                                                                    RealTime
                                                                                                                                    LiveTime
                                                                                                                                    DeadTime
                                                                                                                                    Channel
StreamItemsPrefix                      No default value                                                     String               Each item defined in StreamItems property will be saved in the stream (Nexus, CSV,...) with the Prefix defined here.
SpoolID                                TO_BE_DEFINED                                                        String               Used only by the FlyScan application
__MemorizedConfigurationAlias          TO_BE_DEFINED                                                        String               Only the device could modify this property. The User should never change this property
__MemorizedRoisAlias                   TO_BE_DEFINED                                                        String               Only the device could modify this property. The User should never change this property
__MemorizedNumChannel                  -1                                                                   uLong                 Only the device could modify this property. The User should never change this property
__MemorizedPresetType                  FIXED_REAL                                                           String               Only the device could modify this property. The User should never change this property
__MemorizedPresetValue                 1                                                                    Double               Only the device could modify this property. The User should never change this property
__MemorizedFileGeneration              False                                                                Boolean              Only the device could modify this property. The User should never change this property
__MemorizedStreamType                  NO_STREAM                                                            String               Only the device could modify this property. The User should never change this property
__MemorizedStreamTargetPath            TO_BE_DEFINED                                                        String               Only the device could modify this property. The User should never change this property
__MemorizedStreamTargetFile            TO_BE_DEFINED                                                        String               Only the device could modify this property. The User should never change this property
__MemorizedStreamNbDataPerAcq          2048                                                                 uLong                 Only the device could modify this property. The User should never change this property
__MemorizedStreamNbAcqPerFile          1                                                                    uLong                 Only the device could modify this property. The User should never change this property
__ExpertStreamMemoryMode               COPY                                                                 String               Available only for Nexus format : set the SetDataItemMemoryMode(). Available values : 
                                                                                                                                 - COPY
                                                                                                                                 - NO_COPY
__ExpertStreamWriteMode                ASYNCHRONOUS                                                         String               Only an expert User could change this property. Applicable for StreamNexus Only !Available Values : 
                                                                                                                                 - ASYNCHRONOUS {default} 
                                                                                                                                 - SYNCHRONOUS
__ExpertMemoryMonitor                  False                                                                String               Enable/Disable the creation of a memory file in order to monitor the process memory usage. Example : 
                                                                                                                                    FALSE {by default } (means do not create a memory file) 
                                                                                                                                    TRUE [;c:\my_location\my_file.txt] (means create my_file.txt in c:\my_location)             
__ExpertAcquisitionCollectPeriod       5                                                                    uShort               Define the period of the acquisition collect task.[in ms]
====================================== ==================================================================== ==================== ===============================================


Commands
````````````````````

=========================== =============== ======================= ===========================================
Command name                Arg. in         Arg. out                Description
=========================== =============== ======================= ===========================================
Init                        Void            Void                    Do not use (Use Init of the Generic device)
State                       Void            uLong                    Return the device state
Status                      Void            String                  Return the device state as a string
Snap                        Void            String                  Starts the acquisition of N frames and stops at the end
Stop                        Void            String                  Stops the acquisition at user's request
LoadConfigFile              String          Void                    Load a new config file (.ini) designed by its associated alias. This will download firmware and initialize the boards/modules in the system
SaveConfigFile              Void            Void                    Config file (.ini) can be updated at any time using this command.
SetRoisFromList             Array of String Void                    Return the list of Rois configured and stored in Rois device property
SetRoisFromFile             String          Void                    Define a set of rois for each channel. The set of rois is read from a file indicated by its alias.
GetRois                     Void            Array of String         Returns the list of rois for each channel.
RemoveRois                  uLong            Void                    Remove all Rois for the selected channel. NB: put channel number=-1 to remove all rois for all channels.
StreamResetIndex            Void            Void                    Reset the stream Nexus buffer index to 1
GetDataStreams              Void            Array of String         Return the flyscan data streams associated with this device
GetConfigurationsFilesAlias Void            Array of String         Get the list of alias of Configurations Files (The contents of ConfigurationFiles property).
GetRoisFilesAlias           Void            Array of String         Get the list of alias of Rois Files (The contents of RoisFiles property)
=========================== =============== ======================= ===========================================


Attributes
````````````````````````````

=============================== ======================== ================== ===============================================
Attribute name                  Read/Write               Type               Description
=============================== ======================== ================== ===============================================
boardType                       R                        String             The Board type Fixed by the BoardType property
nbModules                       R                        ULong              Number of Xia boards
nbChannels                      R                        ULong              Number of total of Channels
nbBins                          R                        ULong              Number of bins per channel
currentAlias                    R                        String             Display the current Alias used to load the *.ini* file
currentMode                     R                        String             Display the current Mode : MCA, MAPPING_FULL, MAPPING_SCA
currentConfigFile               R                        String             Display the path+name of current loaded *.ini* file         
peakingTime                     R                        Double             Peaking time of the energy filter, specified in us
dynamicRange                    R/W                      Boolean            Energy range corresponding to 40% of the total ADC range, specified in eV                                                   
fileGeneration                  R/W                      Boolean            Available only for expert user. Enable/Disable writing the data to a file.
triggerLiveTime                 R                        Double             The livetime run statistic as measured by the trigger filter, reported in seconds.
realTime(i)                     R                        Double             The total time that the processor was taking data in seconds
deadTime(i)                     R                        Double             Deadtime as calculated by (ICR - OCR) / ICR
inputCountRate                  R                        Double             This is the total number of triggers divided by the runtime in counts per second.
outputCountRate                 R                        Double             This is the total number of events in the run divided by the runtime in counts per second.    
eventsInRun                     R                        Double             The total number of events in a run that are written into the MCA spectrum.
presetValue                     R/W                      Double             This value is either the number of counts or the time (specified in seconds) it depends on preset_type parameter.
presetType                      R                        Double             Sets the preset run type: "NONE", "FIXED_REAL", "FIXED_LIVE", "FIXED_EVENTS", "FIXED_TRIGGERS"
channel                         R                        ULong              Fluo spectrum for
pixelAdvanceMode                R                        String             Available values : GATE, SYNC
rois(i)_(j)       		        R                        Double             Region Of Interest j for channel i : spectrum sum for roi user input
hrois(i)_(j)                    R                        ULong              Historized roi
channel                         R                        ULong              Fluo spectrum for
streamType                      W                        String             Available only for expert user. The curent stream type. Available values :NO_STREAM - NEXUS_STREAM - CSV_STREAM.
streamTargetPath                W                        String             Available only for expert user. The root path for generated Stream files.
streamTargetFile                W                        String             Available only for expert user. The file name for generated Stream files.
streamNbAcqPerFile              W                        uLong              Available only for expert user. The number of acquisitions for each Stream file.
=============================== ======================== ================== ===============================================


