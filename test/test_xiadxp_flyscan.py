#!/usr/bin/env python
#########################################################
#Arafat NOUREDDINE
#2014/11/19
#Purpose : Test XiaDxp - flyscan mode , i.e mapping with pandabox for triggers
#########################################################
import os
import sys
import PyTango
import time
import datetime

xia_proxy = ''
panda_proxy = ''
#------------------------------------------------------------------------------
# build exception
#------------------------------------------------------------------------------
class BuildError(Exception):
  pass
  
#------------------------------------------------------------------------------
# Colors
#------------------------------------------------------------------------------
class bcolors:
    PINK = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    UNDERLINE = '\033[4m'

def disable(self):
    self.PINK = ''
    self.BLUE = ''
    self.GREEN = ''
    self.YELLOW = ''
    self.FAIL = ''
    self.ENDC = ''
    self.UNDERLINE = ''


#------------------------------------------------------------------------------
def snap(xia_proxy, panda_proxy):
    print '\nSnap() \n------------------'
    #Configure the device    

    #Display time when state is STANDBY (just before load())
    timeBegin = datetime.datetime.now()
    print timeBegin.isoformat(), ' - ', xia_proxy.state()
    xia_proxy.Snap()

    #Display time when state is RUNNING (just after timeSnap())
    timeSnap = datetime.datetime.now()
    print timeSnap.isoformat(), ' - ', xia_proxy.state()
    
    #start triggers through pandabox
    print 'start pandabox, will start triggers\n',
    panda_proxy.Start()
    
    #Loop while state is RUNNING (acquisition in progress...)
    state = xia_proxy.state()
    while (state==PyTango.DevState.RUNNING):
        state = xia_proxy.state()
        if state == PyTango.DevState.STANDBY:
            break
        print '\r', 'Acquisition XiaDxp in progress ...',
        time.sleep(0)

    #Display time when state is STANDBY (just after acquisition is finish)
    timeEnd = datetime.datetime.now()
    print '\n', timeEnd.isoformat(), ' - ', xia_proxy.state()
    print '\nDuration = ', ((timeEnd-timeSnap).total_seconds()*1000),'(ms)'	
    time.sleep(1)	
    
    return
    #return xia_proxy.image


#------------------------------------------------------------------------------
# Usage
#------------------------------------------------------------------------------
def usage():
  print "Usage: [python] test_xiadxp_flyscan.py <xia/device/proxy>  <panda/device/proxy> <nb_loops>"
  sys.exit(1)


#------------------------------------------------------------------------------
# run
#------------------------------------------------------------------------------
def run(xia_proxy_name = 'flyscan/sensor/xiaxdp.1', panda_proxy_name = 'flyscan/clock/pandabox-timebase.1', nb_loops = 1):
    # print arguments
    print '\nProgram inputs :\n--------------'
    print 'xia   proxy_name\t = ', xia_proxy_name
    print 'panda proxy_name\t = ', panda_proxy_name    
    print 'nb_loops\t = ', nb_loops
    xia_proxy = PyTango.DeviceProxy(xia_proxy_name)
    panda_proxy = PyTango.DeviceProxy(panda_proxy_name)    
    #Configure the device
    print '\nConfigure Device attributes :\n--------------'
    xia_proxy.Stop()
    nb_loops = int(nb_loops)
    alias = '1'
    print '\n'
    try:
        current_loop = 0
        while(current_loop<nb_loops):
            print '\n========================================================'
            print '\t' + bcolors.PINK + 'Loop : ', current_loop, bcolors.ENDC,
            print '\n========================================================'
            snap(xia_proxy, panda_proxy)		
            current_loop=current_loop+1
            state = xia_proxy.state()
            if (state!=PyTango.DevState.STANDBY):
            #    raise Exception('FAIL : Acquisition is end with state (%s)' %(state))
			    print bcolors.RED + 'Device is in FAULT !',bcolors.ENDC,
            print '\noutput :\n--------------'
        print '\nProgram outputs :\n--------------'
    except Exception as err:
	   sys.stderr.write('--------------\nERROR :\n--------------\n%s\n' %err)

#------------------------------------------------------------------------------
# Main Entry point
#------------------------------------------------------------------------------
if __name__ == "__main__":
#    if len(sys.argv) < 4:
#        usage()
    print run(*sys.argv[1:])


    
