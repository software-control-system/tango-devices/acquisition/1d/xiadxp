#!/usr/bin/env python
#########################################################
#Arafat NOUREDDINE
#2014/11/19
#Purpose : Test XiaDxp - command snap/stop & state
#########################################################
import os
import sys
import PyTango
import time
import datetime

proxy = ''
#------------------------------------------------------------------------------
# build exception
#------------------------------------------------------------------------------
class BuildError(Exception):
  pass
  
#------------------------------------------------------------------------------
# Colors
#------------------------------------------------------------------------------
class bcolors:
    PINK = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    UNDERLINE = '\033[4m'

def disable(self):
    self.PINK = ''
    self.BLUE = ''
    self.GREEN = ''
    self.YELLOW = ''
    self.FAIL = ''
    self.ENDC = ''
    self.UNDERLINE = ''


#------------------------------------------------------------------------------
def snap(proxy, integration, latency, stop_after):

    print '\nSnap() \n------------------'
    #Configure the device    
    proxy.presetValue = integration
    #Display time when state is STANDBY (just before load())
    timeBegin = datetime.datetime.now()
    print timeBegin.isoformat(), ' - ', proxy.state()
    proxy.Snap()
    
    #get the t0 when snap is executed
    t0_snap = datetime.datetime.now()
    
    #Display time when state is RUNNING (just after timeSnap())
    timeSnap = datetime.datetime.now()
    print timeSnap.isoformat(), ' - ', proxy.state()
    
    #Display time when state is STANDBY (just after acquisition is finish)
    state = proxy.state()
    while (state==PyTango.DevState.RUNNING):
        #compute elapsed time since t0_snap
        t1_snap = datetime.datetime.now()
        t_snap = (t1_snap - t0_snap).total_seconds() #in s
        state = proxy.state()
        #if acquiistion is finish, nothing to do got out 
        if state == PyTango.DevState.STANDBY:
            break
        #if acquisition is running, force the stop if (stop_after) time is elapsed since t0_snap
        if t_snap >= stop_after and stop_after!=0:
            #force stop after (stop_after) 
            print '\nStop() \n------------------'
            proxy.Stop()
            state = proxy.state()
            while (state!=PyTango.DevState.STANDBY):
                state = proxy.state()
                time.sleep(0) 
            break            
        print '\r', 'Acquisition XiaDxp in progress ...',
        time.sleep(0)    
    timeEnd = datetime.datetime.now()
    print '\n', timeEnd.isoformat(), ' - ', proxy.state()
    print '\nDuration = ', ((timeEnd-timeSnap).total_seconds()*1000),'(ms)'	
    time.sleep(latency)	
    
    return
    #return proxy.image


#------------------------------------------------------------------------------
# Usage
#------------------------------------------------------------------------------
def usage():
  print "Usage: [python] test_xiadxp_snap_stop.py <my/device/proxy>  <integration time s> <latency in s> <stop_after in s> <nb_loops>"
  sys.exit(1)


#------------------------------------------------------------------------------
# run
#------------------------------------------------------------------------------
def run(proxy_name = 'tmp/test/xia_memory_leak', integration = 1, latency = 1, stop_after = 1, nb_loops = 1):
    # print arguments
    print '\nProgram inputs :\n--------------'
    print 'proxy_name\t = ', proxy_name
    print 'integration\t = ', integration    
    print 'latency\t = ', latency   
    print 'stop_after\t = ', stop_after    
    print 'nb_loops\t = ', nb_loops
    proxy = PyTango.DeviceProxy(proxy_name)
    #Configure the device
    print '\nConfigure Device attributes :\n--------------'
    proxy.Stop()
    nb_loops = int(nb_loops)
    integration = float(integration)
    latency = float(latency)
    stop_after = float(stop_after)    
    alias = '1'
    print '\n'
    try:
        current_loop = 0
        while(current_loop<nb_loops):
            print '\n========================================================'
            print '\t' + bcolors.PINK + 'Loop : ', current_loop, bcolors.ENDC,
            print '\n========================================================'
            snap(proxy, integration, latency, stop_after)		
            current_loop=current_loop+1
            state = proxy.state()
            if (state!=PyTango.DevState.STANDBY):
            #    raise Exception('FAIL : Acquisition is end with state (%s)' %(state))
			    print bcolors.RED + 'Device is in FAULT !',bcolors.ENDC,
            #print '\noutput :\n--------------'
        #print '\nProgram outputs :\n--------------'
    except Exception as err:
	   sys.stderr.write('--------------\nERROR :\n--------------\n%s\n' %err)

#------------------------------------------------------------------------------
# Main Entry point
#------------------------------------------------------------------------------
if __name__ == "__main__":
#    if len(sys.argv) < 4:
#        usage()
    print run(*sys.argv[1:])


    
