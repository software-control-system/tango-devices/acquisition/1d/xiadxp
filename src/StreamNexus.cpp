
#include <iostream>
#include <iomanip>
#include <fstream>
#include <yat/utils/XString.h>

#include "StreamNexus.h"

namespace XiaDxp_ns
{
#if defined(USE_NX_FINALIZER)
nxcpp::NexusDataStreamerFinalizer StreamNexus::m_data_streamer_finalizer;
bool StreamNexus::m_is_data_streamer_finalizer_started = false;
#endif

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
StreamNexus::StreamNexus(Tango::DeviceImpl *dev)
: Stream(dev), IExceptionHandler(),
m_target_path("TO_BE_DEFINED"),
m_file_name("TO_BE_DEFINED"),
m_write_mode("TO_BE_DEFINED"),
m_nb_acq_per_file(0),
m_nb_bins(0),
m_nb_pixels(0),
m_writer(0)
{
    INFO_STREAM << "StreamNexus::StreamNexus() - [BEGIN]" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
#if defined(USE_NX_FINALIZER)
    if ( ! StreamNexus::m_is_data_streamer_finalizer_started )
    {
        DEBUG_STREAM << "Start NexusDataStreamerFinalizer - [BEGIN]" << std::endl;
        StreamNexus::m_data_streamer_finalizer.start();
        StreamNexus::m_is_data_streamer_finalizer_started = true;
        DEBUG_STREAM << "Start NexusDataStreamerFinalizer - [END]" << std::endl;
    }
#endif
    INFO_STREAM << "StreamNexus::StreamNexus() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
StreamNexus::~StreamNexus()
{
    INFO_STREAM << "StreamNexus::~StreamNexus() - [BEGIN]" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
    if (m_writer)
    {
        //- this might be required, in case we could not pass the writer to the finalizer	 
        delete m_writer;
        m_writer = 0;
    }
    INFO_STREAM << "StreamNexus::~StreamNexus() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::init(yat::SharedPtr<DataStore> data_store)
{
    INFO_STREAM << "StreamNexus::init() - [BEGIN]" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
    m_store = data_store;
    m_channel_names.clear();
    m_triggers_names.clear();
    m_outputs_names.clear();
    m_icr_names.clear();
    m_ocr_names.clear();
    m_realtime_names.clear();
    m_livetime_names.clear();
    m_deadtime_names.clear();
    int total_channels = data_store->get_nb_modules() * data_store->get_nb_channels();
    m_nb_pixels = data_store->get_nb_pixels();
	m_nb_bins = data_store->get_nb_bins();

    INFO_STREAM << "- Create DataStreamer :" << endl;
    INFO_STREAM << "\t- target path = " 	<< m_target_path << endl;
    INFO_STREAM << "\t- file name = "		<< m_file_name << endl;
    INFO_STREAM << "\t- memory mode = " 	<< m_memory_mode << endl;	
    INFO_STREAM << "\t- write mode = " 		<< m_write_mode << endl;
    INFO_STREAM << "\t- nb map pixels = "	<< m_nb_pixels << endl;
    INFO_STREAM << "\t- nb data per acq = "	<< m_nb_bins << endl;
    INFO_STREAM << "\t- nb acq per file = "	<< m_nb_acq_per_file << endl;

    INFO_STREAM << "\t- write data for : ";
	for(unsigned i = 0;i < m_stream_items.size();i++)
    {
        INFO_STREAM << "\t\t. " << m_stream_items.at(i) << endl;
    }
    INFO_STREAM << endl;
	
    try
    {
        m_writer = new nxcpp::DataStreamer(m_file_name, m_nb_pixels, m_nb_acq_per_file);

		DEBUG_STREAM << "- SetExceptionHandler()" << endl;
        m_writer->SetExceptionHandler(this);

        // configure the Memory mode 
		nxcpp::DataStreamer::MemoryMode memory_mode;
		if (m_memory_mode == "NO_COPY" )
		{
			INFO_STREAM << "- Configure the Memory mode : NO_COPY" << endl;
			memory_mode = nxcpp::DataStreamer::NO_COPY;
		}
		else//by default is COPY
		{
			INFO_STREAM << "- Configure the Memory mode : COPY" << endl;
			memory_mode = nxcpp::DataStreamer::COPY;
		}
		
        // configure the Writer mode 
        if (m_write_mode == "SYNCHRONOUS" )
        {
            INFO_STREAM << "- Configure the Writer mode : SYNCHRONOUS" << endl;		
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::SYNCHRONOUS);
        }
        else//by default is ASYNCHRONOUS	
        {
            INFO_STREAM << "- Configure the Writer mode : ASYNCHRONOUS" << endl;
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::ASYNCHRONOUS);
        }
		
        DEBUG_STREAM << "- Initialize()" << endl;
        m_writer->Initialize(m_target_path);	
		
        DEBUG_STREAM << "- Prepare Data Items :" << endl;
        for (int ichan = 0; ichan < total_channels; ichan++)
        {
            //- Channels        
            if ( m_is_channel_enabled )
            {
				std::string str_name = m_stream_items_prefix + "channel%02d";
                m_channel_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                DEBUG_STREAM << "\t- AddDataItem1D(" << m_channel_names[ichan] << "," << m_nb_bins << ")" << endl;
                m_writer->AddDataItem1D(m_channel_names[ichan], m_nb_bins);
				m_writer->SetDataItemMemoryMode(m_channel_names[ichan], memory_mode);
            }

            //- Triggers
            if ( m_is_triggers_enabled )
            {
				std::string str_name = m_stream_items_prefix + "triggers%02d";
                m_triggers_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                DEBUG_STREAM << "\t- AddDataItem0D(" << m_triggers_names[ichan] << ")" << endl;
                m_writer->AddDataItem0D(m_triggers_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_triggers_names[ichan], memory_mode);
            }

            //- events in run
            if ( m_is_outputs_enabled )
            {
				std::string str_name = m_stream_items_prefix + "outputs%02d";
                m_outputs_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                DEBUG_STREAM << "\t- AddDataItem0D(" << m_outputs_names[ichan] << ")" << endl;
                m_writer->AddDataItem0D(m_outputs_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_outputs_names[ichan], memory_mode);				
            }

            //- ICRs
            if ( m_is_icr_enabled )
            {
				std::string str_name = m_stream_items_prefix + "icr%02d";
                m_icr_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                DEBUG_STREAM << "\t- AddDataItem0D(" << m_icr_names[ichan] << ")" << endl;
                m_writer->AddDataItem0D(m_icr_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_icr_names[ichan], memory_mode);				
            }

            //- OCRs
            if ( m_is_ocr_enabled )
            {
				std::string str_name = m_stream_items_prefix + "ocr%02d";
                m_ocr_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                DEBUG_STREAM << "\t- AddDataItem0D(" << m_ocr_names[ichan] << ")" << endl;
                m_writer->AddDataItem0D(m_ocr_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_ocr_names[ichan], memory_mode);				
            }

            //- Realtimes
            if ( m_is_realtime_enabled )
            {
				std::string str_name = m_stream_items_prefix + "realtime%02d";
                m_realtime_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                DEBUG_STREAM << "\t- AddDataItem0D(" << m_realtime_names[ichan] << ")" << endl;
                m_writer->AddDataItem0D(m_realtime_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_realtime_names[ichan], memory_mode);				
            }

            //- Livetimes
            if ( m_is_livetime_enabled )
            {
				std::string str_name = m_stream_items_prefix + "livetime%02d";
                m_livetime_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                DEBUG_STREAM << "\t- AddDataItem0D(" << m_livetime_names[ichan] << ")" << endl;
                m_writer->AddDataItem0D(m_livetime_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_livetime_names[ichan], memory_mode);				
            }

            //- Deadtimes
            if ( m_is_deadtime_enabled )
            {
				std::string str_name = m_stream_items_prefix + "deadtime%02d";
                m_deadtime_names.push_back(yat::String::str_format(str_name.c_str(), ichan));
                DEBUG_STREAM << "\t- AddDataItem0D(" << m_deadtime_names[ichan] << ")" << endl;
                m_writer->AddDataItem0D(m_deadtime_names[ichan]);
				m_writer->SetDataItemMemoryMode(m_deadtime_names[ichan], memory_mode);				
            }
        }
	
    }
    catch(const nxcpp::NexusException &ex)
    {
        Tango::Except::throw_exception((ex.errors[0].reason).c_str(),
                                       (ex.errors[0].desc).c_str(),
                                       (ex.errors[0].origin).c_str());
    }

    INFO_STREAM << "StreamNexus::init() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::open()
{
    INFO_STREAM << "StreamNexus::open() - [BEGIN]" << endl;
    INFO_STREAM << "StreamNexus::open() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::close()
{
    INFO_STREAM << "StreamNexus::close() - [BEGIN]" << endl;
    yat::MutexLock scoped_lock(m_data_lock);            
    if (m_writer)
    {        
#if defined (USE_NX_FINALIZER) 
        //- Use NexusFinalizer to optimize the finalize which was extremly long !!!
        DEBUG_STREAM << "NexusDataStreamerFinalizer - [BEGIN]" << std::endl;
        nxcpp::NexusDataStreamerFinalizer::Entry *e = new nxcpp::NexusDataStreamerFinalizer::Entry();
        e->data_streamer = m_writer;
        m_writer = 0;
        StreamNexus::m_data_streamer_finalizer.push(e);
        DEBUG_STREAM << "NexusDataStreamerFinalizer - [END]" << std::endl;
#else  
        DEBUG_STREAM << "- Finalize() - [BEGIN]" << endl;
        m_writer->Finalize();
        delete m_writer;
        m_writer = 0;
        DEBUG_STREAM << "- Finalize() - [END]" << endl;
#endif 
    }

    INFO_STREAM << "StreamNexus::close() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::abort()
{
    INFO_STREAM << "StreamNexus::abort() - [BEGIN]" << endl;
    yat::MutexLock scoped_lock(m_data_lock);    
    if (m_writer)
    {        
        DEBUG_STREAM << "- Stop() - [BEGIN]" << endl;
        m_writer->Stop();
        delete m_writer;
        m_writer = 0;
        DEBUG_STREAM << "- Stop() - [END]" << endl;
    }
    INFO_STREAM << "StreamNexus::abort() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_target_path(const std::string & target_path)
{
    INFO_STREAM << "StreamNexus::set_target_path(" << target_path << ")" << endl;
    m_target_path = target_path;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_file_name(const std::string & file_name)
{
    INFO_STREAM << "StreamNexus::set_file_name(" << file_name << ")" << endl;
    m_file_name = file_name;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_memory_mode(const std::string & memory_mode)
{
    INFO_STREAM << "StreamNexus::set_memory_mode(" << memory_mode << ")" << endl;
    m_memory_mode = memory_mode;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_write_mode(const std::string & write_mode)
{
    INFO_STREAM << "StreamNexus::set_write_mode(" << write_mode << ")" << endl;
    m_write_mode = write_mode;
}


//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::set_nb_acq_per_file(int nb_acq_per_file)
{
    INFO_STREAM << "StreamNexus::set_nb_acq_per_file(" << nb_acq_per_file << ")" << endl;
    m_nb_acq_per_file = nb_acq_per_file;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::store_statistics(int module,
                                   int channel,
                                   int pixel,
                                   unsigned long triggers,
                                   unsigned long outputs,
                                   double icr,
                                   double ocr,
                                   double realtime,
                                   double livetime,
                                   double deadtime)
{
    //DEBUG_STREAM << "StreamNexus::store_statistics()" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
    if (m_writer)
    {
        //- triggers
        if ( m_is_triggers_enabled )
        {
            m_writer->PushData(m_triggers_names[channel], &triggers);
        }

        //- outputs
        if ( m_is_outputs_enabled )
        {
            m_writer->PushData(m_outputs_names[channel], &outputs);
        }

        //- icr
        if ( m_is_icr_enabled )
        {
            m_writer->PushData(m_icr_names[channel], &icr);
        }

        //- ocr
        if ( m_is_ocr_enabled )
        {
            m_writer->PushData(m_ocr_names[channel], &ocr);
        }

        //- realtime
        if ( m_is_realtime_enabled )
        {
            m_writer->PushData(m_realtime_names[channel], &realtime);
        }

        //- livetime
        if ( m_is_livetime_enabled )
        {
            m_writer->PushData(m_livetime_names[channel], &livetime);
        }

        //- deadtime
        if ( m_is_deadtime_enabled )
        {
            m_writer->PushData(m_deadtime_names[channel], &deadtime);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::store_data(int module, int channel, int pixel, DataType* data, size_t length)
{
    //DEBUG_STREAM << "StreamNexus::store_data()" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
    if (m_writer)
    {
        //- channel
        if ( m_is_channel_enabled )
        {
            m_writer->PushData(m_channel_names[channel], data);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::update_data(int ichannel, yat::SharedPtr<DataStore> data_store)
{
//    DEBUG_STREAM << "StreamNexus::update_data() - [pixel = " << data_store->get_current_pixel(ichannel) << "]" << endl;

    //DEBUG_STREAM<<"\t- Store statistics : channel "<<ichannel<<endl;
    yat::MutexLock scoped_lock(m_data_lock);
    store_statistics(data_store->get_nb_modules(),
                     ichannel,
                     data_store->get_nb_pixels(),
                     data_store->get_triggers(ichannel),
                     data_store->get_outputs(ichannel),
                     data_store->get_input_count_rate(ichannel),
                     data_store->get_output_count_rate(ichannel),
                     data_store->get_realtime(ichannel),
                     data_store->get_livetime(ichannel),
                     data_store->get_deadtime(ichannel)
                     );


    //DEBUG_STREAM<<"\t- Store data\t   : channel "<<ichannel<<endl;
    store_data(data_store->get_nb_modules(),
               ichannel,
               data_store->get_nb_pixels(),
               (DataType*) & data_store->get_channel_data(ichannel)[0],
               data_store->get_channel_data(ichannel).size()
               );
		
	/*
	//- Display Nexus statistics
	nxcpp::DataStreamer::Statistics nxsStats;
	nxsStats = m_writer->GetStatistics();
	DEBUG_STREAM << "Nexus Writer Statistics : ";
	DEBUG_STREAM << "\t- WrittenBytes = "			<< nxsStats.ui64WrittenBytes;
	DEBUG_STREAM << "\t- PendingBytes = "			<< nxsStats.ui64PendingBytes;
	DEBUG_STREAM << "\t- MaxPendingBytes = "        << nxsStats.ui64MaxPendingBytes;
	DEBUG_STREAM << "\t- TotalBytes = "				<< nxsStats.ui64TotalBytes;
	DEBUG_STREAM << "\t- ActiveWriters = "			<< nxsStats.ui16ActiveWriters;
	DEBUG_STREAM << "\t- MaxSimultaneousWriters = "	<< nxsStats.ui16MaxSimultaneousWriters;
	DEBUG_STREAM << "\t- fInstantMbPerSec = "		<< nxsStats.fInstantMbPerSec;
	DEBUG_STREAM << "\t- fPeakMbPerSec = "			<< nxsStats.fPeakMbPerSec;
	DEBUG_STREAM << "\t- fAverageMbPerSec = "		<< nxsStats.fAverageMbPerSec;		
	*/	
    //INFO_STREAM<<" "<<endl;    
//    DEBUG_STREAM << "StreamNexus::update_data() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//	
//----------------------------------------------------------------------------------------------------------------------
void StreamNexus::reset_index()
{
    INFO_STREAM << "StreamNexus::reset_index() - [BEGIN]" << endl;
    DEBUG_STREAM << "- ResetBufferIndex()" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
    nxcpp::DataStreamer::ResetBufferIndex();
    INFO_STREAM << "StreamNexus::reset_index() - [END]" << endl;
}

}// namespace
