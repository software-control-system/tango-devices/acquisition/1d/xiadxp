//=============================================================================
//
// file :        DataParserMapping.cpp
//
// description : 
//
// project :	XiaDxp Project
//
// $Author: noureddine $
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//=============================================================================

#include <tango.h>
#include "Controller.h"
#include "DataStore.h"
#include "DataParser.h"
#include "DataParserMapping.h"


namespace XiaDxp_ns
{
//----------------------------------------------------------------------------------------------------------------------
//- ctor
//----------------------------------------------------------------------------------------------------------------------
DataParserMapping::DataParserMapping(Tango::DeviceImpl *dev, DataStore* store)
: DataParser(dev, store)
{
    INFO_STREAM << "DataParserMapping::DataParserMapping() - [BEGIN]" << endl;
    INFO_STREAM << "DataParserMapping::DataParserMapping() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- dtor
//----------------------------------------------------------------------------------------------------------------------
DataParserMapping::~DataParserMapping()
{
    INFO_STREAM << "DataParserMapping::~DataParserMapping() - [BEGIN]" << endl;
    INFO_STREAM << "DataParserMapping::~DataParserMapping() - [END]" << endl;
}
//----------------------------------------------------------------------------------------------------------------------
//-
//----------------------------------------------------------------------------------------------------------------------
void DataParserMapping::parse_buffer(DataBufferContainerPtr map_buffer_ptr)
{
    DEBUG_STREAM << "DataParserMapping::parse_buffer() - [BEGIN]" << endl;
	DEBUG_STREAM << "\t- parse the MAPPING buffer ..." << endl;
	if(m_board_type == XMAP_TYPE)
	{
		parse_buffer_xmap(map_buffer_ptr);
	}
	else if(m_board_type == FALCONX_TYPE)
	{
		parse_buffer_falconx(map_buffer_ptr);
	}
	else //SIMULATOR_TYPE
	{
		parse_buffer_xmap(map_buffer_ptr);
	}
		
	DEBUG_STREAM << "DataParserMapping::parse_buffer() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- Parse entire buffer acquisition
//- to (XMAP_USER_MANUAL.pdf - 5.3.3.3 Mapping Mode 1 - page 70)
//----------------------------------------------------------------------------------------------------------------------
void DataParserMapping::parse_buffer_xmap(DataBufferContainerPtr map_buffer_ptr)
{
	DEBUG_STREAM << "DataParserMapping::parse_buffer_xmap() - [BEGIN]" << endl;

    try
    {
        if ((map_buffer_ptr->base()[0] != TAG_HEAD0) || (map_buffer_ptr->base()[1] != TAG_HEAD1))
        {
            std::stringstream ss_msg;
            ss_msg << "Missing TAG_HEAD0/TAG_HEAD1 in the mapping buffer !" << std::endl;
			ERROR_STREAM << "DataParserMapping::parse_buffer_xmap() - " << ss_msg.str() << std::endl;
            //stop_acquisition();//@@TODO
            //on_alarm(ss_msg.str());
			m_store->set_status(ss_msg.str());
            m_store->set_state(Tango::FAULT);
            Tango::Except::throw_exception("XIA_ERROR",
                                           (ss_msg.str()).c_str(),
										"DataParserMapping::parse_buffer_xmap()");
        }

        if (map_buffer_ptr->base()[31] != 0x0000)
        {
            std::stringstream ss_msg;
            ss_msg << "Expected 0 in WORD 31 of mapping buffer !" << std::endl;
			ERROR_STREAM << "DataParserMapping::parse_buffer_xmap() - " << ss_msg.str() << std::endl;
            //stop_acquisition();//@@TODO
            //on_alarm(ss_msg.str());
			m_store->set_status(ss_msg.str());			
            m_store->set_state(Tango::FAULT);
            Tango::Except::throw_exception("XIA_ERROR",
                                           (ss_msg.str()).c_str(),
											"DataParserMapping::parse_buffer_xmap()");
        }

        // parse pixels
        int startingPixel = WORD_TO_LONG((int) map_buffer_ptr->base()[9], (int) map_buffer_ptr->base()[10]);
        int nbPixels = (int) map_buffer_ptr->base()[8];
        //INFO_STREAM << "\t- pixels to process : " << nbPixels <<std::endl;

        for (int pixel = 0; pixel < nbPixels; ++pixel)
        {
            //DEBUG_STREAM<<"------------------------------------"<<std::endl;
            //DEBUG_STREAM<<"---------- pixel = "<<pixel<<std::endl;
            //DEBUG_STREAM<<"------------------------------------"<<std::endl;
			if(m_mode == "MAPPING" || m_mode == "MAPPING_FULL")
				parse_pixel_full_xmap(map_buffer_ptr->module(), pixel, map_buffer_ptr->base());
            else
				parse_pixel_sca_xmap(map_buffer_ptr->module(), pixel, map_buffer_ptr->base());
        }

        //INFO_STREAM << "\t- processed pixels : " << startingPixel << " to "  << startingPixel + nbPixels - 1 << " (" <<nbPixels<<")"<<std::endl;
    }
    catch(const std::exception& ex)
    {
		ERROR_STREAM << "DataParserMapping::parse_buffer_xmap() - " << ex.what() << endl;
        //stop_acquisition();//@@TODO
        //on_alarm(ex.what());
		m_store->set_status(ex.what());		
        m_store->set_state(Tango::FAULT);
        Tango::Except::throw_exception("XIA_ERROR",
                                       ex.what(),
									"DataParserMapping::parse_buffer_xmap()");
    }
	DEBUG_STREAM << "DataParserMapping::parse_buffer_xmap() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- Parse one pixel (in mode full) contained in the buffer acquisition
//- to (XMAP_USER_MANUAL.pdf - 5.3.3.3 Mapping Mode 1 - page 70)
//----------------------------------------------------------------------------------------------------------------------
void DataParserMapping::parse_pixel_full_xmap(int module, int pixel, DataType* data_type_buffer)
{
    //DEBUG_STREAM<<"DataParserMapping::parse_pixel_full() - [BEGIN]"<<std::endl;

    DataType* pixel_data = data_type_buffer + BUFFER_HEADER_SIZE;
	
    // go to current pixel
    for (int i = 0; i < pixel; ++i)
    {
		unsigned long length_of_pixel_data = WORD_TO_LONG(pixel_data[6], pixel_data[7]);
		pixel_data += length_of_pixel_data;
    }

	// check pixel buffer tags
    if ((pixel_data[0] != TAG_DATA0) && (pixel_data[1] != TAG_DATA1))
    {
        std::stringstream ss_msg;
        ss_msg << "Missing TAG_DATA0/TAG_DATA1 in the pixel ["<<pixel<<"] of the mapping buffer !" << std::endl;
        ERROR_STREAM << "DataStore::parse_data() - " << ss_msg.str() << endl;
        //stop_acquisition();//@@TODO
        //on_alarm(ss_msg.str());
		m_store->set_status(ss_msg.str());			
        m_store->set_state(Tango::FAULT);
        Tango::Except::throw_exception("XIA_ERROR",
                                       (ss_msg.str()).c_str(),
                                       "DataStore::parse_data()");
    }

    unsigned long the_pixel = WORD_TO_LONG(pixel_data[4], pixel_data[5]);
    //DEBUG_STREAM<<"\t- the_pixel = "<<the_pixel<<std::endl;

	// Get spectrum sizes located at WORD 8/9/10/11 
	// number of channels = ALWAYS 4 for XMAP board !  
	DataType size[NB_CHANNEL_XMAP_MAX];
	for(int channel = 0;channel < NB_CHANNEL_XMAP_MAX;channel++)
    {
        size[channel] = pixel_data[8 + channel];
        //DEBUG_STREAM<<"\t- size channel["<<channel<<"] = "<<size[channel]<<std::endl;
    }

    //DEBUG_STREAM <<"\t- parse data - module  = "  <<module <<" - pixel   = "  <<the_pixel <<" - length  = "  <<length_of_pixData <<std::endl;

    // statistics for current pixel
    //DEBUG_STREAM << "\t- push statistics for the pixel ["<<the_pixel<<"] into DataStore" <<std::endl;

	for(int channel = 0;channel < NB_CHANNEL_XMAP_MAX;channel++)
    {
        //each channels statistics contains 8 WORD, that is why channel*8
        unsigned long realtime = WORD_TO_LONG(pixel_data[32 + channel * 8], pixel_data[33 + channel * 8]);
        unsigned long livetime = WORD_TO_LONG(pixel_data[34 + channel * 8], pixel_data[35 + channel * 8]);
        unsigned long triggers = WORD_TO_LONG(pixel_data[36 + channel * 8], pixel_data[37 + channel * 8]);
        unsigned long outputs  = WORD_TO_LONG(pixel_data[38 + channel * 8], pixel_data[39 + channel * 8]);

        PixelData pix_data;
        pix_data.triggers = triggers;
        pix_data.outputs = outputs;
        pix_data.realtime = static_cast<double> (realtime);
        pix_data.livetime = static_cast<double> (livetime);

        //push statistics into DataStore
        m_store->store_statistics(	module,               //nb module
									channel,              //numero of channel
									the_pixel,            //numero of pixel
									pix_data);
    }

    //datas for current pixel
    //DEBUG_STREAM << "\t- push buffer data for the pixel ["<<the_pixel<<"] into DataStore "<<endl;
    DataType* spectrum_data = pixel_data + PIXEL_HEADER_SIZE;
	for(int channel = 0;channel < NB_CHANNEL_XMAP_MAX;channel++)
    {  
        m_store->store_data(module,                   //nb module
							channel,                  //numero of channel
						    the_pixel,                //numero of pixel
						    (DataType*) spectrum_data,
						    size[channel]);
							
        spectrum_data += size[channel];
    }

    //DEBUG_STREAM << "\t- The pixel ["<<m_current_pixel<<"] is parsed & stored." <<std::endl;
    //DEBUG_STREAM<<"DataParserMapping::parse_pixel_full() - [END]"<<std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- Parse one pixel (in mode sca) contained in the buffer acquisition
//- according to (XMAP_USER_MANUAL.pdf - 5.3.3.3 Mapping Mode 1 - page 71)
//----------------------------------------------------------------------------------------------------------------------
void DataParserMapping::parse_pixel_sca_xmap(int module, int pixel, DataType* data_type_buffer)
{
    //DEBUG_STREAM<<"DataParserMapping::parse_pixel_sca() - [BEGIN]"<<std::endl;

    DataType* pixel_data = data_type_buffer + BUFFER_HEADER_SIZE;
    // go to current pixel
    for (int i = 0; i < pixel; ++i)
    {
		unsigned long length_of_pixel_data = WORD_TO_LONG(pixel_data[6], pixel_data[7]);
		//INFO_STREAM<<"\t\t- length_of_pixel_data = "<<length_of_pixel_data<<endl;
		pixel_data += length_of_pixel_data;
    }

	// check pixel buffer tags
    if ((pixel_data[0] != TAG_DATA0) && (pixel_data[1] != TAG_DATA1))
    {
        std::stringstream ss_msg;
        ss_msg << "Missing TAG_DATA0/TAG_DATA1 in the pixel ["<<pixel<<"] of the mapping buffer !" << std::endl;
        ERROR_STREAM << "DataParserMapping::parse_pixel_sca() - " << ss_msg.str() << endl;
        //stop_acquisition();//@@TODO
        //on_alarm(ss_msg.str());
		m_store->set_status(ss_msg.str());
        m_store->set_state(Tango::FAULT);
        Tango::Except::throw_exception("XIA_ERROR",
                                       (ss_msg.str()).c_str(),
                                       "DataParserMapping::parse_pixel_sca()");
    }

	unsigned long the_pixel_header_size = pixel_data[2];
	//INFO_STREAM<<"\t\t- the_pixel_header_size = "<<the_pixel_header_size<<std::endl;
	
    unsigned long the_pixel = WORD_TO_LONG(pixel_data[4], pixel_data[5]);
	//INFO_STREAM<<"\t\t- the_pixel = "<<the_pixel<<std::endl;
	
	unsigned long the_mapping_mode = pixel_data[3];
	//INFO_STREAM<<"\t\t- the_mapping_mode = "<<the_mapping_mode<<std::endl;

	unsigned long the_roi_size = pixel_data[12];
	//INFO_STREAM<<"\t\t- the_roi_size = "<<the_roi_size<<std::endl;
	
	
	// Get spectrum sizes located at WORD 8/9/10/11 
	// number of channels = ALWAYS 4 for XMAP board 
	DataType nb_rois[NB_CHANNEL_XMAP_MAX];
	for(int channel = 0;channel < NB_CHANNEL_XMAP_MAX;channel++)
    {
        nb_rois[channel] = pixel_data[8 + channel];
		//INFO_STREAM<<"\t\t- nb_rois["<<channel<<"] = "<<nb_rois[channel]<<std::endl;
    }

    //INFO_STREAM <<"\t\t- parse data - module  = "  <<module <<" - pixel   = "  <<the_pixel <<" - length  = "  <<length_of_pixData <<std::endl;

    // statistics for current pixel
    //INFO_STREAM << "\t\t- push statistics for the pixel ["<<the_pixel<<"] into DataStore" <<std::endl;

	for(int channel = 0;channel < NB_CHANNEL_XMAP_MAX;channel++)
    {
        //each channels statistics contains 8 WORD, that is why channel*8
        unsigned long realtime = WORD_TO_LONG(pixel_data[32 + channel * 8], pixel_data[33 + channel * 8]);
        unsigned long livetime = WORD_TO_LONG(pixel_data[34 + channel * 8], pixel_data[35 + channel * 8]);
        unsigned long triggers = WORD_TO_LONG(pixel_data[36 + channel * 8], pixel_data[37 + channel * 8]);
        unsigned long outputs  = WORD_TO_LONG(pixel_data[38 + channel * 8], pixel_data[39 + channel * 8]);

        PixelData pix_data;
        pix_data.triggers = triggers;
        pix_data.outputs = outputs;
        pix_data.realtime = static_cast<double> (realtime);
        pix_data.livetime = static_cast<double> (livetime);

        //push statistics into DataStore
        m_store->store_statistics(module,               //nb module
                         channel,              //numero of channel
                         the_pixel,            //numero of pixel
                         pix_data);
    }

    //datas for current pixel
    //INFO_STREAM << "\t\t- push buffer data for the pixel ["<<the_pixel<<"] into DataStore "<<endl;
    DataType* sca_rois = pixel_data + NB_SCA_MAX;//PIXEL_HEADER_SIZE in case of sca mode = 64
	for(int channel = 0;channel < NB_CHANNEL_XMAP_MAX;channel++)
    {
		for(int i = 0;i < 2*nb_rois[channel];i=i+2)
		{			
			unsigned long sca_rois_num = WORD_TO_LONG(sca_rois[i], sca_rois[i+1]);
			sca_rois[i/2] = sca_rois_num;
		}

        m_store->store_data(module,                   //nb module
                   channel,                  //numero of channel
                   the_pixel,                //numero of pixel
                   (DataType*) sca_rois,
                   nb_rois[channel]
                   );
		sca_rois += 2*nb_rois[channel];
    }

    //DEBUG_STREAM << "\t- The pixel ["<<the_pixel<<"] is parsed & stored." <<std::endl;
    //DEBUG_STREAM<<"DataParserMapping::parse_pixel_sca() - [END]"<<std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- Parse entire buffer acquisition
//- according to http://xia.com/Software/docs/handel-sitoro/1.1.14/handel-falconxn.html#mapping-mode
//----------------------------------------------------------------------------------------------------------------------
void DataParserMapping::parse_buffer_falconx(DataBufferContainerPtr map_buffer_ptr)
{
	DEBUG_STREAM << "DataParserMapping::parse_buffer_falconx() - [BEGIN]" << endl;
	try
	{

		if(WORD_FROM_ARRAY(map_buffer_ptr->base(), 0) != TAG_HEAD0 || WORD_FROM_ARRAY(map_buffer_ptr->base(), 1) != TAG_HEAD1)
		{
			std::stringstream ss_msg;
			ss_msg << "Missing TAG_HEAD0/TAG_HEAD1 in the mapping buffer !" << std::endl;
			ERROR_STREAM << "DataParserMapping::parse_buffer_falconx() - " << ss_msg.str() << std::endl;
			//stop_acquisition();//@@TODO
			//on_alarm(ss_msg.str());
			m_store->set_status(ss_msg.str());
			m_store->set_state(Tango::FAULT);
			Tango::Except::throw_exception("XIA_ERROR",
										(ss_msg.str()).c_str(),
										"DataParserMapping::parse_buffer_falconx()");
		}

		if(WORD_FROM_ARRAY(map_buffer_ptr->base(), 3) != 1)
		{
			std::stringstream ss_msg;
			ss_msg << "Expected (Mapping Mode = 1) in WORD 3 of mapping buffer !" << std::endl;
			ERROR_STREAM << "DataParserMapping::parse_buffer_falconx() - " << ss_msg.str() << std::endl;
			//stop_acquisition();//@@TODO
			//on_alarm(ss_msg.str());
			m_store->set_status(ss_msg.str());
			m_store->set_state(Tango::FAULT);
			Tango::Except::throw_exception("XIA_ERROR",
										(ss_msg.str()).c_str(),
										"DataParserMapping::parse_buffer_falconx()");
		}

		// parse pixels        
		int channel = (int) WORD_FROM_ARRAY(map_buffer_ptr->base(), 12);
		//INFO_STREAM << "\t- channel to process : " << channel <<std::endl;
		int startingPixel = WORD_TO_LONG((int) WORD_FROM_ARRAY(map_buffer_ptr->base(), 9), (int) WORD_FROM_ARRAY(map_buffer_ptr->base(), 10));
		//INFO_STREAM << "\t- starting pixels to process : " << startingPixel <<std::endl;		
		int nbPixels = (int) WORD_FROM_ARRAY(map_buffer_ptr->base(), 8);
		//INFO_STREAM << "\t- nb pixels to process : " << nbPixels <<std::endl;


		for(int pixel = 0;pixel < nbPixels;++pixel)
		{
			//            DEBUG_STREAM<<"------------------------------------"<<std::endl;
			//            DEBUG_STREAM<<"---------- pixel = "<<pixel<<std::endl;
			//            DEBUG_STREAM<<"------------------------------------"<<std::endl;
			if(m_mode == "MAPPING" || m_mode == "MAPPING_FULL")
				parse_pixel_full_falconx(map_buffer_ptr->module(), pixel, map_buffer_ptr->base());
		}

		//INFO_STREAM << "\t- processed pixels : " << startingPixel << " to "  << startingPixel + nbPixels - 1 << " (" <<nbPixels<<")"<<std::endl;
	}
	catch(const std::exception& ex)
	{
		ERROR_STREAM << "DataParserMapping::parse_buffer_falconx() - " << ex.what() << endl;
		//stop_acquisition();//@@TODO
		//on_alarm(ex.what());
		m_store->set_status(ex.what());
		m_store->set_state(Tango::FAULT);
		Tango::Except::throw_exception("XIA_ERROR",
									ex.what(),
									"DataParserMapping::parse_buffer_falconx()");
	}
	DEBUG_STREAM << "DataParserMapping::parse_buffer_falconx() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- Parse one pixel (in mode full) contained in the buffer acquisition
//----------------------------------------------------------------------------------------------------------------------
void DataParserMapping::parse_pixel_full_falconx(int module, int pixel, DataType* data_type_buffer)
{
	//DEBUG_STREAM<<"DataParserMapping::parse_pixel_full_falconx() - [BEGIN]"<<std::endl;

	DataType* pixel_data = data_type_buffer + BUFFER_HEADER_SIZE / 2;

	// go to current pixel
	for(int i = 0;i < pixel;++i)
	{
		unsigned long pixelBlockSize = WORD_TO_LONG(WORD_FROM_ARRAY(pixel_data, 6), WORD_FROM_ARRAY(pixel_data, 7));
		pixel_data += pixelBlockSize / 2;
	}

	// check pixel buffer
	if((WORD_FROM_ARRAY(pixel_data, 0) != TAG_DATA0) && (WORD_FROM_ARRAY(pixel_data, 1) != TAG_DATA1))
	{
		std::stringstream ss_msg;
		ss_msg << "Missing TAG_DATA0/TAG_DATA1 in the pixel [" << pixel << "] of the pixel data !" << std::endl;
		ERROR_STREAM << "DataStore::parse_pixel_full_falconx() - " << ss_msg.str() << endl;
		//stop_acquisition();//@@TODO
		//on_alarm(ss_msg.str());
		m_store->set_status(ss_msg.str());
		m_store->set_state(Tango::FAULT);
		Tango::Except::throw_exception("XIA_ERROR",
									(ss_msg.str()).c_str(),
									"DataStore::parse_pixel_full_falconx()");
	}

	if(WORD_FROM_ARRAY(pixel_data, 3) != 1)
	{
		std::stringstream ss_msg;
		ss_msg << "Expected (Mapping Mode = 1) in WORD 3 of the pixel data !" << std::endl;
		ERROR_STREAM << "DataParserMapping::parse_pixel_full_falconx() - " << ss_msg.str() << std::endl;
		//stop_acquisition();//@@TODO
		//on_alarm(ss_msg.str());
		m_store->set_status(ss_msg.str());
		m_store->set_state(Tango::FAULT);
		Tango::Except::throw_exception("XIA_ERROR",
									(ss_msg.str()).c_str(),
									"DataParserMapping::parse_pixel_full_falconx()");
	}

	unsigned long the_pixel = WORD_TO_LONG(WORD_FROM_ARRAY(pixel_data, 4), WORD_FROM_ARRAY(pixel_data, 5));
	//INFO_STREAM<<"\t- the_pixel = "<<the_pixel<<std::endl;
	unsigned long total_pixel_block_size = WORD_TO_LONG(WORD_FROM_ARRAY(pixel_data, 6), WORD_FROM_ARRAY(pixel_data, 7));
	//INFO_STREAM<<"\t- total pixel block size = "<<total_pixel_block_size<<std::endl;

	// Get spectrum size (the buffer is always related  to one channel)
	unsigned long size_channel = WORD_FROM_ARRAY(pixel_data, 8) / 2;
	//INFO_STREAM<<"\t- size channel = "<<size_channel<<std::endl;


	//INFO_STREAM <<"\t- parse data - module  = "  <<module <<" - pixel   = "  <<the_pixel <<" - length  = "  <<total_pixel_block_size <<std::endl;

	// statistics for current pixel
	//INFO_STREAM << "\t- push statistics for the pixel ["<<the_pixel<<"] into DataStore" <<std::endl;

	//each channels statistics contains 8 WORD, that is why channel*8
	unsigned long realtime = WORD_TO_LONG(WORD_FROM_ARRAY(pixel_data, 32), WORD_FROM_ARRAY(pixel_data, 33));
	unsigned long livetime = WORD_TO_LONG(WORD_FROM_ARRAY(pixel_data, 34), WORD_FROM_ARRAY(pixel_data, 35));
	unsigned long triggers = WORD_TO_LONG(WORD_FROM_ARRAY(pixel_data, 36), WORD_FROM_ARRAY(pixel_data, 37));
	unsigned long outputs = WORD_TO_LONG(WORD_FROM_ARRAY(pixel_data, 38), WORD_FROM_ARRAY(pixel_data, 39));

	PixelData pix_data;
	pix_data.triggers = triggers;
	pix_data.outputs = outputs;
	pix_data.realtime = static_cast<double> (realtime);
	pix_data.livetime = static_cast<double> (livetime);

	//push statistics into DataStore
	m_store->store_statistics(0, //numero of module
							module, //numero of channel
							the_pixel, //numero of pixel
							pix_data);

	//datas for current pixel
	//INFO_STREAM << "\t- push buffer data for the pixel ["<<the_pixel<<"] into DataStore "<<endl;
	DataType* spectrum_data = pixel_data + PIXEL_HEADER_SIZE / 2;
	m_store->store_data(0, //numero of module
						module, //numero of channel
						the_pixel, //numero of pixel
						(DataType*) spectrum_data,
						size_channel
						);

	//INFO_STREAM << "\t- The pixel ["<<the_pixel<<"] is parsed & stored." <<std::endl;
	//DEBUG_STREAM<<"DataParserMapping::parse_pixel_full_falconx() - [END]"<<std::endl;
}



} // namespace 
