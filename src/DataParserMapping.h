/*************************************************************************
/*! 
 *  \file   DataParserMapping.h
 *  \brief  class DataStore
 *  \author Arafat Noureddine
 */
/*************************************************************************/

#ifndef DATA_PARSER_MAPPING_H
#define DATA_PARSER_MAPPING_H

//TANGO
#include <tango.h>
#include "DataStore.h"


namespace XiaDxp_ns
{
//------------------------------------------------------------------
//	class:	DataParserMapping
//------------------------------------------------------------------
class DataParserMapping: public DataParser
{
public:
    ///ctor
    DataParserMapping(Tango::DeviceImpl *dev, DataStore* store);
    ///dtor
    virtual ~DataParserMapping();
    ///Parse entire buffer acquisition
    void parse_buffer(DataBufferContainerPtr map_buffer_ptr);
protected:
    ///parse datas for acquired mapping buffer
    void parse_buffer_xmap(DataBufferContainerPtr map_buffer_ptr);
    ///parse datas for each acquired mapping pixel
    void parse_pixel_full_xmap(int module, int pixel, DataType* data_type_buffer);
    ///parse datas for each acquired mapping pixel
    void parse_pixel_sca_xmap(int module, int pixel, DataType* data_type_buffer);
    ///parse datas for acquired mapping buffer
    void parse_buffer_falconx(DataBufferContainerPtr map_buffer_ptr);
    ///parse datas for each acquired mapping pixel
    void parse_pixel_full_falconx(int module, int pixel, DataType* data_type_buffer);
};

} 

#endif //DATA_PARSER_MAPPING_H
