//=============================================================================
//
// file :        DataStore.cpp
//
// description : 
//
// project :	XiaDxp Project
//
// $Author: noureddine $
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//=============================================================================

#include <tango.h>
#include "Controller.h"
#include "DataStore.h"
#include "DataParser.h"
#include "DataParserMapping.h"
#include "DataParserMca.h"



namespace XiaDxp_ns
{
//----------------------------------------------------------------------------------------------------------------------
//- ctor
//----------------------------------------------------------------------------------------------------------------------
DataStore::DataStore(Tango::DeviceImpl *dev)
: yat4tango::DeviceTask(dev),
  m_device(dev)
{
    INFO_STREAM << "DataStore::DataStore() - [BEGIN]" << endl;
    enable_timeout_msg(false);
    enable_periodic_msg(false);
    set_periodic_msg_period(CONTROLLER_TASK_PERIODIC_MS);
	m_board_type = "XMAP";
    m_nb_modules = 0;
    m_nb_channels = 0;
    m_nb_channels_per_module = 0;	
    m_nb_pixels = 0;
    m_nb_bins = 0;
    m_acquisition_mode = "MCA";
    m_timebase = 1;
    m_is_exception_stream_occured = false;
    set_status("");
    set_state(Tango::OFF);
    INFO_STREAM << "DataStore::DataStore() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- dtor
//----------------------------------------------------------------------------------------------------------------------
DataStore::~DataStore()
{
    INFO_STREAM << "DataStore::~DataStore() - [BEGIN]" << endl;
    INFO_STREAM << "DataStore::~DataStore() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::init
//----------------------------------------------------------------------------------------------------------------------
void DataStore::init(const std::string& board_type, int nb_modules, int nb_channels, int nb_channels_per_module, int nb_pixels, int nb_bins, const std::string& acquisition_mode)
{
    INFO_STREAM << "DataStore::init() - [BEGIN]" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
	m_board_type = board_type;
    m_nb_modules = nb_modules;
    m_nb_channels = nb_channels;
	m_nb_channels_per_module = nb_channels_per_module;
    m_nb_pixels = nb_pixels;
    m_nb_bins = nb_bins;
    m_is_exception_stream_occured = false;
    m_acquisition_mode = acquisition_mode;
	DEBUG_STREAM << "m_nb_modules = " << m_nb_modules << endl;
	DEBUG_STREAM << "m_nb_channels = " << m_nb_channels << endl;
	DEBUG_STREAM << "m_nb_channels_per_module = " << m_nb_channels_per_module << endl;
	
	DEBUG_STREAM << "m_nb_pixels = " << m_nb_pixels << endl;
	DEBUG_STREAM << "m_nb_bins = " << m_nb_bins << endl;
    m_data.module_data.clear();

    //build the data parser
    build_parser(m_acquisition_mode);

    // setup mapping data structure
    m_data.module_data.resize(nb_modules);
    for (int imod = 0; imod < nb_modules; imod++)
    {
        m_data.module_data[imod].module = imod;
        m_data.module_data[imod].channel_data.resize(nb_channels);

        for (int ich = 0; ich < nb_channels; ich++)
        {
            m_data.module_data[imod].channel_data[ich].channel = ich;

            // only the last pixel is stored            
            m_data.module_data[imod].channel_data[ich].pixel_data.pixel = -1;
            m_data.module_data[imod].channel_data[ich].pixel_data.trigger_livetime = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.triggers = 0;       //for MAPPING (identical to events_in_run : read from WORD(36,37) )
            m_data.module_data[imod].channel_data[ich].pixel_data.outputs = 0;
            m_data.module_data[imod].channel_data[ich].pixel_data.events_in_run = 0;  //for MCA (identical to triggers : get_run_data("events_in_run")
            m_data.module_data[imod].channel_data[ich].pixel_data.input_count_rate = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.output_count_rate = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.deadtime = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.realtime = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.livetime = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.data.resize(m_nb_bins);//max of bins (2048, 4096, ...)
            m_data.module_data[imod].channel_data[ich].pixel_data.sca_rois.resize(NB_SCA_MAX);//max of scas/channel = 64
        }
    }
    set_state(Tango::STANDBY);
    INFO_STREAM << "DataStore::init() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::reinit
//----------------------------------------------------------------------------------------------------------------------
void DataStore::reinit()
{
    INFO_STREAM << "DataStore::reinit() - [BEGIN]" << endl;    
    yat::MutexLock scoped_lock(m_data_lock);
    m_is_exception_stream_occured = false;


    // setup mapping data structure
    m_data.module_data.clear();
    m_data.module_data.resize(m_nb_modules);
	DEBUG_STREAM << "m_nb_modules = " << m_nb_modules << endl;
	DEBUG_STREAM << "m_nb_channels = " << m_nb_channels << endl;
	DEBUG_STREAM << "m_nb_channels_per_module = " << m_nb_channels_per_module << endl;
	DEBUG_STREAM << "m_nb_pixels = " << m_nb_pixels << endl;
	DEBUG_STREAM << "m_nb_bins = " << m_nb_bins << endl;
    for (int imod = 0; imod < m_nb_modules; imod++)
    {
        m_data.module_data[imod].module = imod;
        m_data.module_data[imod].channel_data.resize(m_nb_channels);

        for (int ich = 0; ich < m_nb_channels; ich++)
        {
            m_data.module_data[imod].channel_data[ich].channel = ich;

            // only the last pixel is stored
            m_data.module_data[imod].channel_data[ich].pixel_data.pixel = -1;
            m_data.module_data[imod].channel_data[ich].pixel_data.trigger_livetime = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.triggers = 0;       //for MAPPING (identical to events_in_run : read from WORD(36,37) )
            m_data.module_data[imod].channel_data[ich].pixel_data.outputs = 0;
            m_data.module_data[imod].channel_data[ich].pixel_data.events_in_run = 0;  //for MCA (identical to triggers : get_run_data("events_in_run")
            m_data.module_data[imod].channel_data[ich].pixel_data.input_count_rate = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.output_count_rate = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.deadtime = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.realtime = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.livetime = 0.0;
            m_data.module_data[imod].channel_data[ich].pixel_data.data.resize(m_nb_bins);//max of bins (2048, 4096, ...)
            m_data.module_data[imod].channel_data[ich].pixel_data.sca_rois.resize(NB_SCA_MAX);//max of scas/channel = 64
        }
    }

    set_state(Tango::STANDBY);
    INFO_STREAM << "DataStore::reinit() - [END]" << endl;    
}

// ============================================================================
// DataStore::build_parser()
// ============================================================================
void DataStore::build_parser(const std::string& acq_mode)
{
    INFO_STREAM << "DataStore::build_parser() - [BEGIN]" << endl;
    try
    {
        std::string mode = acq_mode;
        std::transform(mode.begin(), mode.end(), mode.begin(), ::toupper);

        //create the stream
        if(mode == "MAPPING"||mode == "MAPPING_FULL"||mode == "MAPPING_SCA")
        {
            m_parser.reset(new DataParserMapping(m_device, this));
        }
        else if(mode == "MCA")
        {
            m_parser.reset(new DataParserMca(m_device, this));
        }
        else
        {
            std::ostringstream ossMsgErr;
            ossMsgErr   <<  "Wrong Acquisition mode:\n"
            "Possibles values are:\n"
            "MAPPING\n"
            "MAPPING_FULL\n"
            "MAPPING_SCA\n"
            "MCA"
            <<std::endl;
            Tango::Except::throw_exception("LOGIC_ERROR",
                                           ossMsgErr.str().c_str(),
                                           "DataStore::build_parser()");
        }
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        std::stringstream ss;
        ss << "Initialization Failed.\n" << endl;
        ss << "Origin\t: " << df.errors[0].origin << endl;
        ss << "Desc\t: " << df.errors[0].desc << endl;
        on_abort(ss.str());

    }
    INFO_STREAM << "DataStore::build_parser() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::store_statistics
//----------------------------------------------------------------------------------------------------------------------
void DataStore::store_statistics(int module, int channel, int pixel, PixelData pix_data)
{
    DEBUG_STREAM << "DataStore::store_statistics() - [BEGIN]" << endl;
	DEBUG_STREAM << "module = " << module << " - channel = " << channel << " - pixel = " << pixel << endl;

    yat::MutexLock scoped_lock(m_data_lock);
    set_state(Tango::RUNNING);
    double computed_realtime = 0.0;
    double computed_livetime = 0.0;
    double computed_icr = 0.0;
    double computed_ocr = 0.0;
    double computed_deadtime = 0.0;
    double computed_trigger_livetime = 0.0;
    if(m_acquisition_mode == "MAPPING" ||m_acquisition_mode == "MAPPING_FULL"||m_acquisition_mode == "MAPPING_SCA")
    {
        computed_deadtime = compute_deadtime(pix_data.outputs, pix_data.triggers, pix_data.livetime, pix_data.realtime); //100*(1 - outputs*livetime/(realtime*triggers))
        computed_realtime = compute_realtime(pix_data.realtime);
        computed_livetime = compute_livetime(pix_data.livetime);
        computed_icr = compute_input_count_rate(pix_data.triggers, computed_livetime);
        computed_ocr = compute_output_count_rate(pix_data.outputs, computed_realtime);
        computed_trigger_livetime = 0.0; //always =0.0
    }
    else if(m_acquisition_mode == "MCA")
    {
        computed_deadtime = compute_deadtime(pix_data.output_count_rate, pix_data.input_count_rate); //100* (icr - ocr)/icr
        computed_realtime = pix_data.realtime;
        computed_livetime = pix_data.livetime;
        computed_icr = pix_data.input_count_rate;
        computed_ocr = pix_data.output_count_rate;
        computed_trigger_livetime = pix_data.trigger_livetime;
    }

    m_data.module_data[module].channel_data[channel].pixel_data.pixel = pixel;
    m_data.module_data[module].channel_data[channel].pixel_data.triggers = pix_data.triggers;
    m_data.module_data[module].channel_data[channel].pixel_data.outputs = pix_data.outputs;
    m_data.module_data[module].channel_data[channel].pixel_data.trigger_livetime = computed_trigger_livetime;
    m_data.module_data[module].channel_data[channel].pixel_data.events_in_run = pix_data.events_in_run;
    m_data.module_data[module].channel_data[channel].pixel_data.input_count_rate = computed_icr;
    m_data.module_data[module].channel_data[channel].pixel_data.output_count_rate = computed_ocr;
    m_data.module_data[module].channel_data[channel].pixel_data.deadtime = computed_deadtime;
    m_data.module_data[module].channel_data[channel].pixel_data.realtime = computed_realtime;
    m_data.module_data[module].channel_data[channel].pixel_data.livetime = computed_livetime;
    DEBUG_STREAM << "DataStore::store_statistics() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::store_data
//----------------------------------------------------------------------------------------------------------------------
void DataStore::store_data(int module, int channel, int pixel, DataType* data, size_t length, bool is_last_mca)
{
    DEBUG_STREAM << "DataStore::store_data() - [BEGIN]" << endl;
	DEBUG_STREAM << "module = " << module << " - channel = " << channel << " - pixel = " << pixel << endl;
    yat::MutexLock scoped_lock(m_data_lock);
	int channel_cluster = TO_CHANNEL_CLUSTER(module, channel);
    set_state(Tango::RUNNING);
    m_data.module_data[module].channel_data[channel].pixel_data.pixel = pixel;
    if(m_acquisition_mode == "MAPPING" ||m_acquisition_mode == "MAPPING_FULL")
    {
		std::copy(data, data + length, &m_data.module_data[module].channel_data[channel].pixel_data.data[0]);
		//inform observers that data is changed	
		notify_data(channel_cluster); //data+view    
    }
    else if(m_acquisition_mode == "MAPPING_SCA")
    {        
        m_data.module_data[module].channel_data[channel].pixel_data.sca_rois.resize(length);//resize because nb_roi of a channel can changed often
        if(length!=0)
            std::copy(data, data + length, &m_data.module_data[module].channel_data[channel].pixel_data.sca_rois[0]);
		//inform observers that data is changed	
		notify_data(channel_cluster); //data+view    			
    }
    else if(m_acquisition_mode == "MCA")
    {
        std::copy(data, data + length, &m_data.module_data[module].channel_data[channel].pixel_data.data[0]);
		//inform observers that data is changed	
		notify_data(channel_cluster, is_last_mca, true); //data if last + view
    }
    
    DEBUG_STREAM << "DataStore::store_data() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_current_pixel
//----------------------------------------------------------------------------------------------------------------------
long   DataStore::get_current_pixel(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    long current_pixel;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    current_pixel = m_data.module_data[module].channel_data[channel_of_module].pixel_data.pixel;
    return current_pixel;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_channel_data
//----------------------------------------------------------------------------------------------------------------------
std::vector<DataType> DataStore::get_channel_data(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    std::vector<DataType> pixelData = m_data.module_data[module].channel_data[channel_of_module].pixel_data.data;
    return pixelData;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_hroi_data
//----------------------------------------------------------------------------------------------------------------------
std::vector<DataType> DataStore::get_hroi_data(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    std::vector<DataType> pixelData = m_data.module_data[module].channel_data[channel_of_module].pixel_data.sca_rois;
    return pixelData;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_trigger_livetime
//----------------------------------------------------------------------------------------------------------------------
double DataStore::get_trigger_livetime(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double trigger_livetime;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    trigger_livetime = m_data.module_data[module].channel_data[channel_of_module].pixel_data.trigger_livetime;
    return trigger_livetime;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_realtime
//----------------------------------------------------------------------------------------------------------------------
double DataStore::get_realtime(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double realtime;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    realtime = m_data.module_data[module].channel_data[channel_of_module].pixel_data.realtime;
    return realtime;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_livetime
//----------------------------------------------------------------------------------------------------------------------
double DataStore::get_livetime(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double livetime;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    livetime = m_data.module_data[module].channel_data[channel_of_module].pixel_data.livetime;
    return livetime;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_deadtime
//----------------------------------------------------------------------------------------------------------------------
double DataStore::get_deadtime(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double deadtime;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    deadtime = m_data.module_data[module].channel_data[channel_of_module].pixel_data.deadtime;
    return deadtime;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_output_count_rate
//----------------------------------------------------------------------------------------------------------------------
double DataStore::get_output_count_rate(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double count_rate;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    count_rate = m_data.module_data[module].channel_data[channel_of_module].pixel_data.output_count_rate;
    return count_rate;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_input_count_rate
//----------------------------------------------------------------------------------------------------------------------
double DataStore::get_input_count_rate(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    double count_rate;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    count_rate = m_data.module_data[module].channel_data[channel_of_module].pixel_data.input_count_rate;
    return count_rate;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_events
//----------------------------------------------------------------------------------------------------------------------
unsigned long DataStore::get_events_in_run(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    unsigned long events_in_run = 0;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    if(m_acquisition_mode == "MCA")
        events_in_run = m_data.module_data[module].channel_data[channel_of_module].pixel_data.events_in_run;
    else if(m_acquisition_mode == "MAPPING" ||m_acquisition_mode == "MAPPING_FULL"||m_acquisition_mode == "MAPPING_SCA")
        events_in_run = m_data.module_data[module].channel_data[channel_of_module].pixel_data.triggers;

    return events_in_run;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_triggers
//----------------------------------------------------------------------------------------------------------------------
unsigned long DataStore::get_triggers(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    unsigned long triggers;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    triggers = m_data.module_data[module].channel_data[channel_of_module].pixel_data.triggers;
    return triggers;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_outputs
//----------------------------------------------------------------------------------------------------------------------
unsigned long DataStore::get_outputs(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    unsigned long outputs;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    outputs = m_data.module_data[module].channel_data[channel_of_module].pixel_data.outputs;
    return outputs;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_nb_modules
//----------------------------------------------------------------------------------------------------------------------
int DataStore::get_nb_modules()
{
    yat::MutexLock scoped_lock(m_data_lock);
    return m_nb_modules;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_nb_channels
//----------------------------------------------------------------------------------------------------------------------
int DataStore::get_nb_channels()
{
    yat::MutexLock scoped_lock(m_data_lock);
    return m_nb_channels;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_nb_channels_per_module
//----------------------------------------------------------------------------------------------------------------------
int DataStore::get_nb_channels_per_module()
{
    yat::MutexLock scoped_lock(m_data_lock);
    return m_nb_channels_per_module;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_nb_pixels
//----------------------------------------------------------------------------------------------------------------------
int DataStore::get_nb_pixels()
{
    yat::MutexLock scoped_lock(m_data_lock);
    return m_nb_pixels;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::set_nb_pixels
//----------------------------------------------------------------------------------------------------------------------
void DataStore::set_nb_pixels(int nb_pixels)
{
    yat::MutexLock scoped_lock(m_data_lock);
    m_nb_pixels = nb_pixels;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_nb_bins
//----------------------------------------------------------------------------------------------------------------------
int DataStore::get_nb_bins()
{
    yat::MutexLock scoped_lock(m_data_lock);
    return m_nb_bins;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_nb_rois
//----------------------------------------------------------------------------------------------------------------------
int   DataStore::get_nb_rois(int channel)
{
    yat::MutexLock scoped_lock(m_data_lock);
    int nb_scas;

	int module = TO_MODULE_AND_CHANNEL(channel).first;
	int channel_of_module = TO_MODULE_AND_CHANNEL(channel).second;

    nb_scas = m_data.module_data[module].channel_data[channel_of_module].pixel_data.sca_rois.size();
    return nb_scas;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_acquisition_mode
//----------------------------------------------------------------------------------------------------------------------
std::string DataStore::get_acquisition_mode()
{
    return m_acquisition_mode;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_acquisition_mode
//----------------------------------------------------------------------------------------------------------------------
std::string DataStore::get_board_type()
{
	return m_board_type;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::get_timebase
//----------------------------------------------------------------------------------------------------------------------
double DataStore::get_timebase()
{
    return m_timebase;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::set_timebase
//----------------------------------------------------------------------------------------------------------------------
void DataStore::set_timebase(double timebase)
{
    m_timebase = timebase;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::compute_realtime
//----------------------------------------------------------------------------------------------------------------------
double DataStore::compute_realtime(double realtime)
{
    //DEBUG_STREAM << "DataStore::compute_realtime()" << endl;
    return realtime * m_timebase;
}
//----------------------------------------------------------------------------------------------------------------------
//- DataStore::compute_livetime
//----------------------------------------------------------------------------------------------------------------------
double DataStore::compute_livetime(double livetime)
{
    //DEBUG_STREAM << "DataStore::compute_livetime()" << endl;
    return livetime * m_timebase;
}
//----------------------------------------------------------------------------------------------------------------------
//- DataStore::compute_input_count_rate
//----------------------------------------------------------------------------------------------------------------------
double DataStore::compute_input_count_rate(unsigned long triggers, double livetime)
{
    //DEBUG_STREAM << "DataStore::compute_input_count_rate()" << endl;
    if (livetime != 0.0)
    {
        return double(triggers) / livetime;
    }
    return 1.0;
}
//----------------------------------------------------------------------------------------------------------------------
//- DataStore::compute_output_count_rate
//----------------------------------------------------------------------------------------------------------------------
double DataStore::compute_output_count_rate(unsigned long outputs, double realtime)
{
    //DEBUG_STREAM << "DataStore::compute_output_count_rate()" << endl;
    if (realtime != 0.0)
    {
        return double(outputs) / realtime;
    }
    return 1.0;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::compute_deadtime (MCA))
//----------------------------------------------------------------------------------------------------------------------
double DataStore::compute_deadtime(double ocr, double icr)
{
    //DEBUG_STREAM << "DataStore::compute_deadtime()" << endl;

    if (icr != 0.0)
    {
        return 100.0 * ((icr - ocr) / icr);
    }
    return 0.0;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::compute_deadtime (MAPPING))
//----------------------------------------------------------------------------------------------------------------------
double DataStore::compute_deadtime(unsigned long outputs, unsigned long triggers, double livetime, double realtime)
{
    //DEBUG_STREAM << "DataStore::compute_deadtime()" << endl;
    if (realtime != 0.0 && triggers != 0)
    {
        return (100.0 * (1 - (1.0 * double(outputs) * livetime / ( realtime * double(triggers)))));
    }
    return 0.0;
}


//----------------------------------------------------------------------------------------------------------------------
//- DataStore::subscribe
//----------------------------------------------------------------------------------------------------------------------
void DataStore::subscribe(Controller* observer)
{
	INFO_STREAM << "DataStore::subscribe(" << observer << ") - [BEGIN]" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
    m_controller = observer;
	INFO_STREAM << "DataStore::subscribe() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::notify_data
//----------------------------------------------------------------------------------------------------------------------
void DataStore::notify_data(int ichannel, bool update_stream , bool update_view )
{
	DEBUG_STREAM << "DataStore::notify_data() - [BEGIN]" << endl;
    yat::MutexLock scoped_lock(m_data_lock);
    if(m_controller != 0)
        m_controller->update_data(ichannel, update_stream, update_view);
	DEBUG_STREAM << "DataStore::notify_data() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
void DataStore::close_data()
{
    INFO_STREAM << "DataStore::close_data() - [BEGIN]" << std::endl;
    post(DATASTORE_CLOSE_DATA_MSG);    
    INFO_STREAM << "DataStore::close_data() - [END]" << std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
void DataStore::abort_data()
{
    INFO_STREAM << "DataStore::abort_data() - [BEGIN]" << std::endl;
    post(DATASTORE_ABORT_DATA_MSG);     
    INFO_STREAM << "DataStore::abort_data() - [END]" << std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
void DataStore::abort(std::string status)
{
    INFO_STREAM << "DataStore::abort() - [BEGIN]" << std::endl;    
    yat::Message* msg = yat::Message::allocate(DATASTORE_ABORT_MSG, DEFAULT_MSG_PRIORITY, true);
    msg->attach_data(status);
    post(msg);
    INFO_STREAM << "DataStore::abort() - [END]" << std::endl;
}
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
void DataStore::process_data(DataBufferContainerPtr map_buffer_ptr)
{
    DEBUG_STREAM << "DataStore::process_data() - [BEGIN]" << std::endl;
	DEBUG_STREAM << "\t- process Data Buffer" << std::endl;
    post(DATASTORE_PROCESS_DATA_MSG, map_buffer_ptr, true);
    DEBUG_STREAM << "DataStore::process_data() - [END]" << std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::on_close_data
//----------------------------------------------------------------------------------------------------------------------
void DataStore::on_close_data()
{
    yat::MutexLock scoped_lock(m_data_lock);
    INFO_STREAM << "DataStore::on_close_data() - [BEGIN]" << endl;

    if(m_controller != 0)
        m_controller->close_stream();

    set_state(Tango::STANDBY);
    INFO_STREAM << "DataStore::on_close_data() - [END]" << endl;
    INFO_STREAM << "--------------------------------------------" << std::endl;
    INFO_STREAM << " " << std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::on_abort_data
//----------------------------------------------------------------------------------------------------------------------
void DataStore::on_abort_data()
{
    yat::MutexLock scoped_lock(m_data_lock);
    INFO_STREAM << "DataStore::on_abort_data() - [BEGIN]" << endl;
    if(m_controller != 0)
        m_controller->abort_stream();
    set_state(Tango::STANDBY);
    INFO_STREAM << "DataStore::on_abort_data() - [END]" << endl;
    INFO_STREAM << "--------------------------------------------" << std::endl;
    INFO_STREAM << " " << std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- DataStore::on_abort
//----------------------------------------------------------------------------------------------------------------------
void DataStore::on_abort(std::string status)
{
    yat::MutexLock scoped_lock(m_data_lock);
    INFO_STREAM << "DataStore::on_abort() - [BEGIN]" << endl;
    set_state(Tango::FAULT);
    set_status(status);
    m_is_exception_stream_occured = true;
    if(m_controller != 0)
        m_controller->stop_acquisition();
    INFO_STREAM << "DataStore::on_abort() - [END]" << endl;
    INFO_STREAM << "--------------------------------------------" << std::endl;
    INFO_STREAM << " " << std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
void DataStore::on_process_data(DataBufferContainerPtr map_buffer_ptr)
{
    DEBUG_STREAM << "DataStore::on_process_data() - [BEGIN]" << std::endl;
    set_state(Tango::RUNNING);
    try
    {
        m_parser->parse_buffer(map_buffer_ptr);
    }
    catch (Tango::DevFailed &df)
    {
        ERROR_STREAM << df << endl;
        set_state(Tango::FAULT);
        Tango::Except::throw_exception("XIA_ERROR",
                                       string(df.errors[0].desc).c_str(),
                                       "DataStore::on_process_data()");
    }

	DEBUG_STREAM << "DataStore::process_data() - [END]" << std::endl;
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
void DataStore::set_state(Tango::DevState state)
{
    {
        //- AutoLock the following
        yat::MutexLock scoped_lock(m_state_lock);
        m_state = state;
    }
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
Tango::DevState DataStore::get_state()
{
    {
        //- AutoLock the following
        yat::MutexLock scoped_lock(m_state_lock);
        return m_state;
    }
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
void DataStore::set_status(const std::string& status)
{
    {
        //- AutoLock the following
        yat::MutexLock scoped_lock(m_state_lock);
        m_status.str("");
        m_status << status.c_str();
    }
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
std::string DataStore::get_status()
{
    {
        //- AutoLock the following
        yat::MutexLock scoped_lock(m_state_lock);
        return (m_status.str());
    }
}

//---------------------------
//- DataStore::process_message()
//---------------------------
void DataStore::process_message(yat::Message& msg) throw (Tango::DevFailed)
{
    DEBUG_STREAM << "DataStore::process_message(): entering... !" << endl;
    try
    {
        switch (msg.type())
        {
                //-----------------------------------------------------
            case yat::TASK_INIT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << "-> DataStore::TASK_INIT" << endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
            }
                break;
                //-----------------------------------------------------
            case yat::TASK_EXIT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << "-> DataStore::TASK_EXIT" << endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
            }
                break;
                //-----------------------------------------------------
            case yat::TASK_TIMEOUT:
            {
                INFO_STREAM << " " << std::endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
                INFO_STREAM << "-> DataStore::TASK_TIMEOUT" << endl;
                INFO_STREAM << "--------------------------------------------" << std::endl;
            }
                break;
                //-----------------------------------------------------------------------------------------------

            case yat::TASK_PERIODIC:
            {
//                DEBUG_STREAM << " " << std::endl;
//                DEBUG_STREAM << "--------------------------------------------" << std::endl;
//                DEBUG_STREAM << "-> DataStore::TASK_PERIODIC" << endl;
//                DEBUG_STREAM << " " << std::endl;
//                DEBUG_STREAM << "--------------------------------------------" << std::endl;
            }
                break;
                //-----------------------------------------------------
            case DATASTORE_PROCESS_DATA_MSG:
            {
//                INFO_STREAM<<" "<<std::endl;
//                INFO_STREAM<<"--------------------------------------------"<<std::endl;
//                INFO_STREAM <<"-> DataStore::DATASTORE_PROCESS_DATA_MSG" << endl;
//                INFO_STREAM<<"--------------------------------------------"<<std::endl;
                try
                {
                    yat::MutexLock scoped_lock(m_data_lock);
                    if(!m_is_exception_stream_occured)
                    {
                        on_process_data(msg.get_data<DataBufferContainerPtr>());
                    }
                }
                catch (Tango::DevFailed &df)
                {
                    ERROR_STREAM << df << endl;
                    set_state(Tango::FAULT);
                }
            }
                break;
                //-----------------------------------------------------
            case DATASTORE_CLOSE_DATA_MSG:
            {
                DEBUG_STREAM<<" "<<std::endl;
                DEBUG_STREAM<<"--------------------------------------------"<<std::endl;
                DEBUG_STREAM <<"-> DataStore::DATASTORE_CLOSE_DATA_MSG" << endl;
                DEBUG_STREAM<<"--------------------------------------------"<<std::endl;
                try
                {
                    yat::MutexLock scoped_lock(m_data_lock);
                    if(!m_is_exception_stream_occured)
                    {
                        on_close_data();
                    }
                }
                catch (Tango::DevFailed &df)
                {
                    ERROR_STREAM << df << endl;
                    set_state(Tango::FAULT);
                }
            }
                break;
                //-----------------------------------------------------			
            case DATASTORE_ABORT_DATA_MSG:
            {
                DEBUG_STREAM<<" "<<std::endl;
                DEBUG_STREAM<<"--------------------------------------------"<<std::endl;
                DEBUG_STREAM <<"-> DataStore::DATASTORE_ABORT_DATA_MSG" << endl;
                DEBUG_STREAM<<"--------------------------------------------"<<std::endl;
                try
                {
                    yat::MutexLock scoped_lock(m_data_lock);
                    if(!m_is_exception_stream_occured)
                    {
                        on_abort_data();
                    }
                }
                catch (Tango::DevFailed &df)
                {
                    ERROR_STREAM << df << endl;
                    set_state(Tango::FAULT);
                }
            }
                break;
                //-----------------------------------------------------			
            case DATASTORE_ABORT_MSG:
            {
                DEBUG_STREAM<<" "<<std::endl;
                DEBUG_STREAM<<"--------------------------------------------"<<std::endl;
                DEBUG_STREAM <<"-> DataStore::DATASTORE_ABORT_MSG" << endl;
                DEBUG_STREAM<<"--------------------------------------------"<<std::endl;
                try
                {
                    yat::MutexLock scoped_lock(m_data_lock);
                    if(!m_is_exception_stream_occured)
                    {
                        std::string status  = msg.get_data<std::string>();
                        on_abort(status);
                    }
                }
                catch (Tango::DevFailed &df)
                {
                    ERROR_STREAM << df << endl;
                    set_state(Tango::FAULT);
                }
            }
                break;
                //-----------------------------------------------------		                
        }
    }
    catch (yat::Exception& ex)
    {
        //- TODO Error Handling
        ex.dump();
        std::stringstream error_msg("");
        error_msg << "Origin\t: " << ex.errors[0].origin << endl;
        error_msg << "Desc\t: " << ex.errors[0].desc << endl;
        error_msg << "Reason\t: " << ex.errors[0].reason << endl;
        set_state(Tango::FAULT);
        ERROR_STREAM << "Exception from - DataStore::process_message() : " << error_msg.str() << endl;
        throw;
    }
}
/*------------------------------------------------------------------
 *	class:	DataBufferContainer
 *	description:	DataStore Factory
/------------------------------------------------------------------*/
DataBufferContainer::DataBufferContainer(int module, std::size_t buffer_length)
: m_module(module)
{    
    m_buffer = new DataBuffer(buffer_length, true /*clear buffer*/); 
}

//----------------------------------------------------------------------------------------------------------------------
//- DataBufferContainer::~DataBufferContainer
//----------------------------------------------------------------------------------------------------------------------
DataBufferContainer::~DataBufferContainer()
{
}

//----------------------------------------------------------------------------------------------------------------------
//- DataBufferContainer::base
//----------------------------------------------------------------------------------------------------------------------
DataType* DataBufferContainer::base() const
{
    return m_buffer->base();
}

//----------------------------------------------------------------------------------------------------------------------
//- DataBufferContainer::module
//----------------------------------------------------------------------------------------------------------------------
int DataBufferContainer::module() const
{
    return m_module;
}


} // namespace 
