/*************************************************************************
/*! 
 *  \file   DataParserMca.h
 *  \brief  class DataParserMca
 *  \author Arafat Noureddine
 */
/*************************************************************************/

#ifndef DATA_PARSER_MCA_H
#define DATA_PARSER_MCA_H

//TANGO
#include <tango.h>
#include "DataStore.h"


namespace XiaDxp_ns
{
//------------------------------------------------------------------
//	class:	DataParserMca
//------------------------------------------------------------------
class DataParserMca: public DataParser
{
public:
    ///ctor
    DataParserMca(Tango::DeviceImpl *dev, DataStore* store);
    ///dtor
    virtual ~DataParserMca();
    ///parse datas for acquired buffer
    void parse_buffer(DataBufferContainerPtr map_buffer) ;
protected:
};

} 

#endif //DATA_PARSER_MCA_H
