//=============================================================================
//
// file :        DataParser.cpp
//
// description : 
//
// project :	XiaDxp Project
//
// $Author: noureddine $
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//=============================================================================

#include <tango.h>
#include "Controller.h"
#include "DataStore.h"
#include "DataParser.h"


namespace XiaDxp_ns
{
//----------------------------------------------------------------------------------------------------------------------
//- ctor
//----------------------------------------------------------------------------------------------------------------------
DataParser::DataParser(Tango::DeviceImpl *dev, DataStore* store)
: Tango::LogAdapter(dev),  
m_device(dev),
m_store(store)
{
    INFO_STREAM << "DataParser::DataParser() - [BEGIN]" << endl;
	m_mode = store->get_acquisition_mode();
	m_board_type = store->get_board_type();
    INFO_STREAM << "DataParser::DataParser() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- dtor
//----------------------------------------------------------------------------------------------------------------------
DataParser::~DataParser()
{
    INFO_STREAM << "DataParser::~DataParser() - [BEGIN]" << endl;
    INFO_STREAM << "DataParser::~DataParser() - [END]" << endl;
}

} // namespace 
