//=============================================================================
//
// file :        AttrViewMapping.cpp
//
// description : 
//
// project :	XiaDxp Project
//
// $Author: noureddine $
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//=============================================================================

#include "AttrViewMapping.h"
#include <XiaDxp.h>

namespace XiaDxp_ns
{
//-----------------------------------------------------------------------------
// method :  AttrViewMapping::AttrViewMapping()
// description : Ctor
//-----------------------------------------------------------------------------
AttrViewMapping::AttrViewMapping(Tango::DeviceImpl *dev)
:AttrView(dev)
{
    INFO_STREAM << "AttrViewMapping::AttrViewMapping() - [BEGIN]" << endl;
	m_dyn_nb_pixels = 0;
    m_dyn_nb_pixels_per_buffer = 0;
    m_dyn_current_pixel = 0;
	m_dyn_pixel_advance_mode = 0;
    INFO_STREAM << "AttrViewMapping::AttrViewMapping() - [END]" << endl;
}

//-----------------------------------------------------------------------------
// method :  AttrViewMapping::~AttrViewMapping()
// description : Dtor
//-----------------------------------------------------------------------------
AttrViewMapping::~AttrViewMapping()
{
    INFO_STREAM << "AttrViewMapping::~AttrViewMapping() - [BEGIN]" << endl;
	delete m_dyn_nb_pixels;
	delete m_dyn_nb_pixels_per_buffer;
	delete m_dyn_current_pixel;
	delete m_dyn_pixel_advance_mode;
	for(unsigned i = 0;i < m_dyn_channel.size();i++)
	{
		delete m_dyn_channel[i];
	}
	m_dyn_channel.clear();


	for(unsigned i = 0;i < m_dyn_hroi.size();++i)
	{
		for(unsigned j = 0;j < m_dyn_hroi[i].size();++j)
		{
			delete m_dyn_hroi[i][j];
		}
		m_dyn_hroi[i].clear();
	}
	m_dyn_hroi.clear();
	
    INFO_STREAM << "AttrViewMapping::~AttrViewMapping() - [END]" << endl;
}

//-----------------------------------------------------------------------------
// method :  AttrViewMapping::init()
// description : Create all dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::init(yat::SharedPtr<DataStore> data_store)
{
    INFO_STREAM << "AttrViewMapping::init() - [BEGIN]" << endl;
    try
    {
		//- remove all previous dyn attributes
		m_dim.dynamic_attributes_manager().remove_attributes();
		//- create common attributes (stats + channels)
		AttrView::init_common_attributes(data_store);

        m_dyn_channel.resize(m_nb_total_channels);
        m_dyn_hroi.resize(m_nb_total_channels);        

		////////////////////////////////////////////////////////////////////////////////////////	
		{
        std::string name = "nbPixels";
        INFO_STREAM << "\t- Create Mapping dynamic attribute [" << name << "]" << endl;
        yat4tango::DynamicAttributeInfo dai;
        dai.dev = m_device;

        //- specify the dyn. attr.  name
        dai.tai.name = name;
        //- associate the dyn. attr. with its data 
        m_dyn_nb_pixels = new LongUserData(name);
        dai.set_user_data(m_dyn_nb_pixels);
        //- describe the dynamic attr we want...
        dai.tai.data_type = Tango::DEV_LONG;
        dai.tai.data_format = Tango::SCALAR;
        dai.tai.writable = Tango::READ_WRITE;
        dai.tai.disp_level = Tango::OPERATOR;
        dai.tai.unit = " ";
        dai.tai.format = "%5d";
        dai.tai.description = "Number of pixels (step) for the Mapping acquisition.";

		//- instanciate the read callback (called when the dyn. attr. is read)    
        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMapping::read_nb_pixels_callback);
		
        //- instanciate the write callback (called when the dyn. attr. is writen)    
        dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrViewMapping::write_nb_pixels_callback);

        //- add the dyn. attr. to the device
        m_dim.dynamic_attributes_manager().add_attribute(dai);
		}

		////////////////////////////////////////////////////////////////////////////////////////	
		{
        std::string name = "currentPixel";
        INFO_STREAM << "\t- Create Mapping dynamic attribute [" << name << "]" << endl;
        yat4tango::DynamicAttributeInfo dai;
        dai.dev = m_device;

        //- specify the dyn. attr.  name
        dai.tai.name = name;
        //- associate the dyn. attr. with its data 	
        m_dyn_current_pixel = new LongUserData(name);
        dai.set_user_data(m_dyn_current_pixel);

        //- describe the dynamic attr we want...
        dai.tai.data_type = Tango::DEV_LONG;
        dai.tai.data_format = Tango::SCALAR;
        dai.tai.writable = Tango::READ;
        dai.tai.disp_level = Tango::OPERATOR;
        dai.tai.unit = " ";
        dai.tai.format = "%5d";
        dai.tai.description = "Current pixel of the mapping.";

        //- instanciate the read callback (called when the dyn. attr. is read)    
        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMapping::read_current_pixel_callback);

        //- add the dyn. attr. to the device
        m_dim.dynamic_attributes_manager().add_attribute(dai);
		}

		////////////////////////////////////////////////////////////////////////////////////////	
		{
        std::string name = "nbPixelsPerBuffer";
        INFO_STREAM << "\t- Create Mapping dynamic attribute [" << name << "]" << endl;
        yat4tango::DynamicAttributeInfo dai;
        dai.dev = m_device;

        //- specify the dyn. attr.  name
        dai.tai.name = name;
        //- associate the dyn. attr. with its data 
        m_dyn_nb_pixels_per_buffer = new LongUserData(name);
        dai.set_user_data(m_dyn_nb_pixels_per_buffer);
        //- describe the dynamic attr we want...
        dai.tai.data_type = Tango::DEV_LONG;
        dai.tai.data_format = Tango::SCALAR;
        dai.tai.writable = Tango::READ_WRITE;
        dai.tai.disp_level = Tango::EXPERT;
        dai.tai.unit = " ";
        dai.tai.format = "%5d";
        dai.tai.description = "Number of pixels per buffer for the Mapping acquisition.";

		//- instanciate the read callback (called when the dyn. attr. is read)    
        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMapping::read_nb_pixels_per_buffer_callback);
		
        //- instanciate the write callback (called when the dyn. attr. is writen)    
        dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrViewMapping::write_nb_pixels_per_buffer_callback);

        //- add the dyn. attr. to the device
        m_dim.dynamic_attributes_manager().add_attribute(dai);
		}
    
		////////////////////////////////////////////////////////////////////////////////////////	
		{
        std::string name = "pixelAdvanceMode";
        INFO_STREAM << "\t- Create Mapping dynamic attribute [" << name << "]" << endl;
        yat4tango::DynamicAttributeInfo dai;
        dai.dev = m_device;

        //- specify the dyn. attr.  name
        dai.tai.name = name;
        //- associate the dyn. attr. with its data 
        m_dyn_pixel_advance_mode = new StringUserData(name);
        dai.set_user_data(m_dyn_pixel_advance_mode);
            //- describe the dynamic attr we want...
        dai.tai.data_type = Tango::DEV_STRING;
        dai.tai.data_format = Tango::SCALAR;
        dai.tai.writable = Tango::READ;////@TODO : Tango::READ_WRITE;
        dai.tai.disp_level = Tango::OPERATOR;
        dai.tai.unit = " ";
        dai.tai.format = "%s";
        dai.tai.description = "Available values :\nGATE\nSYNC\n";

        //- instanciate the read callback (called when the dyn. attr. is read)    
        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMapping::read_pixel_advance_mode_callback);

		//- add the dyn. attr. to the device
		m_dim.dynamic_attributes_manager().add_attribute(dai);
		}

        ////////////////////////////////////////////////////////////////////////////////////////		
        if(m_acquisition_mode == "MAPPING_SCA")//create sca_rois attributes in MAPPNG_SCA mode
        {
            for (int i = 0; i < m_nb_total_channels; i++)
			{
				m_dyn_hroi[i].resize(m_nb_rois[i]);
                DEBUG_STREAM<<"channel_num = "<<i<<" : m_nb_rois = "<<m_nb_rois[i]<<endl;
                for (int j = 0; j < m_nb_rois[i]; j++)
                {
                    double low=0, high=0;
                    dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->get_roi_bounds(i, j, low,high);
                    DEBUG_STREAM<<"channel_num = "<<i<<", roi_num = "<<j<<", low = "<<low<<", high = "<<high<<endl;

                    //- hroi name
                    const string name = yat::String::str_format("hroi%02d_%02d", i, j + 1);

                    INFO_STREAM << "\t- Create Mapping dynamic attribute [" << name << "]" << endl;

                    yat4tango::DynamicAttributeInfo dai;
                    dai.dev = m_device;
                    //- specify the dyn. attr.  name
                    dai.tai.name = name;
                    //- associate the dyn. attr. with its data
                    m_dyn_hroi[i][j] = new HRoiUserData(name, i,j);
                    dai.set_user_data(m_dyn_hroi[i][j]);
                    //- describe the dynamic attr we want...
                    dai.tai.data_type = Tango::DEV_ULONG;
                    dai.tai.data_format = Tango::SPECTRUM;
                    dai.tai.writable = Tango::READ;
                    dai.tai.disp_level = Tango::OPERATOR;
                    dai.tai.unit = "cts";
                    dai.tai.format = "%d";
                    dai.tai.description = "Historized roi";

                    //- instanciate the read callback (called when the dyn. attr. is read)
                    dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMapping::read_hroi_callback);

                    //- add the dyn. attr. to the device
                    m_dim.dynamic_attributes_manager().add_attribute(dai);
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////        
        if(m_acquisition_mode == "MAPPING" || m_acquisition_mode == "MAPPING_FULL")//create channel attributes in MAPPING/MAPPNG_FUIL mode
        {
            for (int i = 0; i < m_nb_total_channels; i++)
            {
                std::stringstream ss("");
                std::string name = "channel";
                ss<<name<<std::setfill('0') << std::setw(2)<<i;

                INFO_STREAM << "\t- Create Mapping Full dynamic attribute [" << ss.str() << "]" << endl;
                yat4tango::DynamicAttributeInfo dai;
                dai.dev = m_device;
                //- specify the dyn. attr.  name
                dai.tai.name = ss.str();
                //- associate the dyn. attr. with its data
                m_dyn_channel[i] = new ChannelUserData(ss.str(), m_nb_bins);
                dai.set_user_data(m_dyn_channel[i]);
                //- describe the dynamic attr we want...
                dai.tai.data_type = Tango::DEV_ULONG;
                dai.tai.data_format = Tango::SPECTRUM;
                dai.tai.writable = Tango::READ;
                dai.tai.disp_level = Tango::OPERATOR;
                dai.tai.unit = "cts";
                dai.tai.format = "%9d";
                dai.tai.description = "Fluo spectrum for " + ss.str();

                //- instanciate the read callback (called when the dyn. attr. is read)
                dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMapping::read_channel_callback);

				//- add the dyn. attr. to the device
				m_dim.dynamic_attributes_manager().add_attribute(dai);
			}
		}
    }
    catch (Tango::DevFailed& df)
    {
        std::string err("failed to instanciate dynamic attributes  - Tango exception caught - see log attribute for details");
        ERROR_STREAM << err << std::endl;
        ERROR_STREAM << df << std::endl;
        return;
    }
    INFO_STREAM << "AttrViewMapping::init() - [END]" << endl;
}

//-----------------------------------------------------------------------------
// method :  AttrViewMapping::reinit()
// description : Clear data in the user data object attached to the dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::reinit()
{
    INFO_STREAM << "AttrViewMapping::reinit() - [BEGIN]" << endl;
    m_dyn_current_pixel->set_value(static_cast<Tango::DevLong>(0));
	for(unsigned i = 0;i < m_dyn_hroi.size();++i)
    {
		for(unsigned j = 0;j < m_dyn_hroi[i].size();++j)
        {
            m_dyn_hroi[i][j]->clear();
        }
    }
    INFO_STREAM << "AttrViewMapping::reinit() - [END]" << endl;
}
//-----------------------------------------------------------------------------
// method :  AttrViewMapping::read_nb_pixels_callback()
// description : Read callback fct for nbPixels dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::read_nb_pixels_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMapping::read_nb_pixels_callback()" << endl; //  << cbd.dya->get_name() << endl;
    try
    {
        Tango::DevState state = static_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
        bool is_device_initialized = static_cast<XiaDxp*>(m_device)->is_device_initialized();
        if ((state == Tango::FAULT && is_device_initialized) ||
            state == Tango::INIT	||
			state == Tango::OFF	||
            state == Tango::DISABLE)
        {
            std::string reason = "It's currently not allowed to read attribute nbPixels";
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           reason.c_str(),
                                           "AttrViewMapping::read_nb_pixels_callback()");
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            "DynamicInterface::read_callback");
        }

        LongUserData* user_data = cbd.dya->get_user_data<LongUserData>();	
		
        //- set the attribute value 
        long current = dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->get_num_map_pixels();
        user_data->set_value(current);
        
        //- set the attribute value
        cbd.tga->set_value((Tango::DevLong*)&user_data->get_value());
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::read_nb_pixels_callback()");
    }
}

//+----------------------------------------------------------------------------
//
// method :         AttrViewMapping::write_nb_pixels_callback()
// description : Write callback fct for nbPixels dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::write_nb_pixels_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMapping::write_nb_pixels_callback()" << endl; //  << cbd.dya->get_name() << endl;
    try
    {
        Tango::DevState state = static_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
        if (state == Tango::FAULT	||
            state == Tango::INIT	||
            state == Tango::RUNNING	||
			state == Tango::OFF	||
            state == Tango::DISABLE)
        {
            std::string reason = "It's currently not allowed to write attribute nbPixels";
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           reason.c_str(),
                                           "AttrViewMapping::write_nb_pixels_callback()");
        }

        //- be sure the pointer to the dyn. attr. is valid		
        if(!cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            "DynamicInterface::write_callback");
        }

        Tango::DevLong val;
        cbd.tga->get_write_value( val);

		LongUserData* user_data = cbd.dya->get_user_data<LongUserData>();        
        user_data->set_value(val);

        static_cast<XiaDxp*>(m_device)->get_ctrl()->set_num_map_pixels(val);      
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::write_nb_pixels_callback()");
    }
}

//-----------------------------------------------------------------------------
// method :         AttrViewMapping::read_nb_pixels_per_buffer_callback()
// description : Read callback fct for nbPixelsPerBuffer dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::read_nb_pixels_per_buffer_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMapping::read_nb_pixels_per_buffer_callback()" << endl; //  << cbd.dya->get_name() << endl;
    try
    {
        Tango::DevState state = static_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
        bool is_device_initialized = static_cast<XiaDxp*>(m_device)->is_device_initialized();
        if ((state == Tango::FAULT  && is_device_initialized) ||
            state == Tango::INIT	||
			state == Tango::OFF     ||
            state == Tango::DISABLE)
        {
            std::string reason = "It's currently not allowed to read attribute nbPixels";
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           reason.c_str(),
                                           "AttrViewMapping::read_nb_pixels_per_buffer_callback()");
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            "DynamicInterface::read_callback");
        }

        LongUserData* user_data = cbd.dya->get_user_data<LongUserData>();	
		
        //- set the attribute value 
        long current = dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->get_num_map_pixels_per_buffer();
        user_data->set_value(current);
        
        //- set the attribute value
        cbd.tga->set_value((Tango::DevLong*)&user_data->get_value());
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::read_nb_pixels_per_buffer_callback()");
    }
}

//-----------------------------------------------------------------------------
// method :         AttrViewMapping::write_nb_pixels_per_buffer_callback()
// description : Write callback fct for nbPixelsPerBuffer dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::write_nb_pixels_per_buffer_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMapping::write_nb_pixels_per_buffer_callback()" << endl; //  << cbd.dya->get_name() << endl;
    try
    {
        Tango::DevState state = static_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
        if (state == Tango::FAULT	||
            state == Tango::INIT	||
            state == Tango::RUNNING	||
			state == Tango::OFF	||
            state == Tango::DISABLE)
        {
            std::string reason = "It's currently not allowed to write attribute nbPixelsPerBuffer";
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           reason.c_str(),
                                           "AttrViewMapping::write_nb_pixels_per_buffer_callback()");
        }

        //- be sure the pointer to the dyn. attr. is valid		
        if(!cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            "DynamicInterface::write_callback");
        }

        Tango::DevLong val;
        cbd.tga->get_write_value( val);

		LongUserData* user_data = cbd.dya->get_user_data<LongUserData>();        
        user_data->set_value(val);

        static_cast<XiaDxp*>(m_device)->get_ctrl()->set_num_map_pixels_per_buffer(val);        
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::write_nb_pixels_callback()");
    }
}

//-----------------------------------------------------------------------------
// method :         AttrViewMapping::write_gap_callback()
// description : Read callback fct for currentPixel dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::read_current_pixel_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMapping::read_current_pixel_callback()" << endl; //  << cbd.dya->get_name() << endl;
    try
    {
        Tango::DevState state = static_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
        bool is_device_initialized = static_cast<XiaDxp*>(m_device)->is_device_initialized();
        if ((state == Tango::FAULT && is_device_initialized) ||
            state == Tango::INIT	||
			state == Tango::OFF	||
            state == Tango::DISABLE)
        {
            std::string reason = "It's currently not allowed to read attribute currentPixel";
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           reason.c_str(),
                                           "AttrViewMapping::read_current_pixel_callback()");
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            "DynamicInterface::read_callback");
        }

        LongUserData* user_data = cbd.dya->get_user_data<LongUserData>();
        //- set the attribute value
        cbd.tga->set_value((Tango::DevLong*)&user_data->get_value());
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::read_current_pixel_callback()");
    }
}

//-----------------------------------------------------------------------------
// method :         AttrViewMca::read_preset_type_callback()
// description : Read callback fct for pixelAdvanceMode dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::read_pixel_advance_mode_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMca::read_pixel_advance_mode_callback()" << endl; //  << cbd.dya->get_name() << endl;
    //- be sure the pointer to the dyn. attr. is valid
    if(!cbd.dya)
    {
        THROW_DEVFAILED("INTERNAL_ERROR",
                        "unexpected NULL pointer to dynamic attribute",
                        "AttrViewMapping::read_pixel_advance_mode_callback");
    }

    try
    {
        Tango::DevState state = dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
        bool is_device_initialized = dynamic_cast<XiaDxp*>(m_device)->is_device_initialized();
        if ((state == Tango::FAULT && is_device_initialized) ||
            state == Tango::INIT	||
			state == Tango::OFF	||
            state == Tango::DISABLE)
        {
            std::string reason = "It's currently not allowed to read attribute pixelAdvanceMode";
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           reason.c_str(),
                                           "AttrViewMapping::read_pixel_advance_mode_callback()");
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            "AttrViewMapping::read_pixel_advance_mode_callback()");
        }

        StringUserData* user_data = cbd.dya->get_user_data<StringUserData>();
        //- set the attribute value 
        string current = dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->get_pixel_advance_mode();
        user_data->set_value(current);
        cbd.tga->set_value((Tango::DevString*)user_data->get_value());
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::read_pixel_advance_mode_callback()");
    }
}

//-----------------------------------------------------------------------------
// method :         AttrViewMapping::write_pixel_advance_mode_callback()
// description : Write callback fct for pixelAdvanceMode dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMapping::write_pixel_advance_mode_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMapping::write_pixel_advance_mode_callback()" << endl; //  << cbd.dya->get_name() << endl;
    try
    {
        Tango::DevState state = dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
        if (state == Tango::FAULT	||
            state == Tango::INIT	||
            state == Tango::RUNNING	||
            state == Tango::OFF	||
            state == Tango::DISABLE)
        {
            std::string reason = "It's currently not allowed to write attribute pixelAdvanceMode";
            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           reason.c_str(),
                                           "AttrViewMapping::write_pixel_advance_mode_callback()");
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            "AttrViewMapping::write_pixel_advance_mode_callback");
        }

		string previous_memorized = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_pixel_advance_mode();
        Tango::DevString val;
        cbd.tga->get_write_value( val);

        string current = val;
        transform(current.begin(), current.end(), current.begin(), ::toupper);
        dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->set_pixel_advance_mode(current);

        StringUserData* user_data = cbd.dya->get_user_data<StringUserData>();
        user_data->set_value(val);
		
		//memorize in the device properties
		if(current != previous_memorized)
			yat4tango::PropertyHelper::set_property(dynamic_cast<XiaDxp*>(m_device), "__MemorizedPixelAdvanceMode", val);
    }
    catch (Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::write_pixel_advance_mode_callback()");
    }
}

//-------------------------------------------------------------------
//	method:	AttrViewMapping::read_channel_callback
//	description: read callback fct for the channel specrum dynamic attrs
//-------------------------------------------------------------------
void AttrViewMapping::read_channel_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMapping::read_channel_callback() : "<<cbd.dya->get_name()<<" - [BEGIN]" << endl;

    Tango::DevState state = dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
    bool is_device_initialized = dynamic_cast<XiaDxp*>(m_device)->is_device_initialized();
    if ((state == Tango::FAULT && is_device_initialized) ||
        state == Tango::INIT	||
        state == Tango::OFF	||
        state == Tango::DISABLE)
    {
        std::string reason = "It's currently not allowed to read attribute : "+cbd.dya->get_name();
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                       reason.c_str(),
                                       "AttrViewMapping::read_channel_callback()");
    }

    //- be sure the pointer to the dyn. attr. is valid
    if(!cbd.dya)
    {
        THROW_DEVFAILED("INTERNAL_ERROR",
                        "unexpected NULL pointer to dynamic attribute",
                        "DynamicInterface::read_callback");
    }

    try
    {
        ChannelUserData* channel_data = cbd.dya->get_user_data<ChannelUserData>();
        //- set the attribute value
        cbd.tga->set_value(const_cast<Tango::DevULong*>(&channel_data->get_value()[0]), channel_data->get_value().size());
    }
    catch(Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::read_channel_callback");
    }

    DEBUG_STREAM << "AttrViewMapping::read_channel_callback() - [END]" << endl;
}

//-------------------------------------------------------------------
//	method:	AttrViewMapping::read_hroi_callback
//	description: Read callback fct for the hroi specrum dynamic attrs
//-------------------------------------------------------------------
void AttrViewMapping::read_hroi_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
    DEBUG_STREAM << "AttrViewMapping::read_hroi_callback() : "<<cbd.dya->get_name()<<" - [BEGIN]" << endl;

    Tango::DevState state = dynamic_cast<XiaDxp*>(m_device)->get_ctrl()->get_state();
    bool is_device_initialized = dynamic_cast<XiaDxp*>(m_device)->is_device_initialized();
    if ((state == Tango::FAULT && is_device_initialized) ||
        state == Tango::INIT	||
        state == Tango::OFF	||
        state == Tango::DISABLE)
    {
        std::string reason = "It's currently not allowed to read attribute : "+cbd.dya->get_name();
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                       reason.c_str(),
                                       "AttrViewMapping::read_hroi_callback()");
    }

    //- be sure the pointer to the dyn. attr. is valid
    if(!cbd.dya)
    {
        THROW_DEVFAILED("INTERNAL_ERROR",
                        "unexpected NULL pointer to dynamic attribute",
                        "DynamicInterface::read_callback");
    }

    try
    {
        HRoiUserData* hroi_data = cbd.dya->get_user_data<HRoiUserData>();
        //- set the attribute value
        if(hroi_data->get_value().size()!=0)
            cbd.tga->set_value(const_cast<Tango::DevULong*>(&hroi_data->get_value()[0]), hroi_data->get_value().size());
    }
    catch(Tango::DevFailed& df)
    {
        ERROR_STREAM << df << endl;
        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "AttrViewMapping::read_hroi_callback");
    }

    DEBUG_STREAM << "AttrView::read_channel_callback() - [END]" << endl;
}

//-----------------------------------------------------------------------------
//	method:	AttrViewMapping::update_data
//	description: update the data of the attributes, this method is fired by the DataStore
//-----------------------------------------------------------------------------
void AttrViewMapping::update_data(int ichannel, yat::SharedPtr<DataStore> data_store)
{
    DEBUG_STREAM << "AttrViewMapping::update_data() - [BEGIN]" << endl;

    DEBUG_STREAM<<"ichannel = "<<ichannel<<endl;
    //- update common attributes (stats + channels)	
    AttrView::update_common_attributes(ichannel, data_store);
	
    //- update currentPixel attribute
    m_dyn_current_pixel->set_value(data_store->get_current_pixel(ichannel)+1);

    if(m_acquisition_mode == "MAPPING" || m_acquisition_mode == "MAPPING_FULL")
    {
        //- update channels attributes
        m_dyn_channel[ichannel]->set_value(data_store->get_channel_data(ichannel));
    }

    if(m_acquisition_mode == "MAPPING_SCA")
    {

        //- update hrois attributes
        for (int i = 0; i < m_nb_rois[ichannel]; ++i)
        {
            m_dyn_hroi[ichannel][i]->set_value(data_store->get_hroi_data(ichannel).at(i));
        }

    }

    DEBUG_STREAM << "AttrViewMapping::update_data() - [END]" << endl;
}

}// namespace
