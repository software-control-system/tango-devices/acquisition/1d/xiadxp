/*************************************************************************
/*! 
 *  \file   AttrViewMca.h
 *  \brief  class AttrViewMca
 *  \author Arafat NOUREDDINE
 */
/*************************************************************************/

#ifndef ATTR_VIEW_MCA_H
#define ATTR_VIEW_MCA_H

#include <tango.h>
#include <yat/memory/SharedPtr.h>
#include <yat4tango/PropertyHelper.h>
#include "AttrView.h"
#include "DataStore.h"

namespace XiaDxp_ns
{
/*------------------------------------------------------------------
 *	class:	AttrViewMca
 *	description: 
 /------------------------------------------------------------------*/
class AttrViewMca : public AttrView
{
public:
    AttrViewMca(Tango::DeviceImpl *dev);
    ~AttrViewMca();
    void init(yat::SharedPtr<DataStore> data_store);
    void reinit();
    void update_data(int ichannel, yat::SharedPtr<DataStore> data_store);

protected:
    /// callback methods for  tango dyn attributes - presetValue
    void write_preset_value_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd);	
	
    /// callback methods for  tango dyn attributes - presetValue
    void read_preset_value_callback(yat4tango::DynamicAttributeReadCallbackData& cbd);		
	
    /// callback methods for  tango dyn attributes - presetType
    void write_preset_type_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd);	
	
    /// callback methods for  tango dyn attributes - presetType
    void read_preset_type_callback(yat4tango::DynamicAttributeReadCallbackData& cbd);		

    /// callback methods for  tango dyn attributes - roi
    void read_roi_callback(yat4tango::DynamicAttributeReadCallbackData& cbd);		    
    
    /// callback methods for common tango dyn attributes - channels attributes
    void read_channel_callback(yat4tango::DynamicAttributeReadCallbackData& cbd);

	///user data attached to the dynamic attributes
	DoubleUserData* m_dyn_preset_value;	    
	StringUserData* m_dyn_preset_type;
    std::vector<ChannelUserData*> 	m_dyn_channel;
    std::vector<std::vector<RoiUserData*> >    m_dyn_roi;    
};

} // namespace 

#endif


