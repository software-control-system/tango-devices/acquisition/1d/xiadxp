/*************************************************************************
/*! 
 *  \file   DataStore.h
 *  \brief  class DataStore
 *  \author Arafat Noureddine - SOLEIL (consultant MEDIANE SYSTEME) /V. Kaiser - ANKA / F. Langlois - SOLEIL
 */
/*************************************************************************/

#ifndef DATA_STORE_H
#define DATA_STORE_H

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Mutex.h>
#include <yat/memory/DataBuffer.h>
#include <yat4tango/DeviceTask.h>
#include <yat/any/Any.h>
#include <yat/time/Timer.h>
#include <vector>
#include <nexuscpp/nexuscpp.h>

const size_t DATASTORE_TASK_PERIODIC_MS      = 100;//ms
const size_t DATASTORE_PROCESS_DATA_MSG      = yat::FIRST_USER_MSG + 600;
const size_t DATASTORE_CLOSE_DATA_MSG        = yat::FIRST_USER_MSG + 601;
const size_t DATASTORE_ABORT_DATA_MSG        = yat::FIRST_USER_MSG + 602;
const size_t DATASTORE_ABORT_MSG             = yat::FIRST_USER_MSG + 603;


///return a WORD at a speific position located in an array 32bits
inline unsigned short WORD_FROM_ARRAY(unsigned long* p, unsigned num)
{
    unsigned short value;
    //    if(m_board_type == "XMAP")
    //    {
    //        value = LO_WORD(p[num]);
    //    }
    //    else
    {
        unsigned idx = num / 2;
        if(num % 2)
            value = (p[idx] >> 16)&0xFFFF;
        else
            value = (p[idx])&0xFFFF;
    }
    return value;
}

namespace XiaDxp_ns
{
typedef unsigned long DataType;
typedef yat::Buffer<DataType> DataBuffer;
typedef yat::SharedPtr<DataBuffer> DataBufferPtr;

//------------------------------------------------------------------
//	Data structures
//------------------------------------------------------------------
class PixelData
{
public:
	PixelData()
	{
		pixel = 0;
		triggers = 0;
		outputs = 0;
		trigger_livetime = 0.0;
		events_in_run = 0;
		input_count_rate = 0.0; //input count rate
		output_count_rate = 0.0; //output count rate
		deadtime = 0.0;
		realtime = 0.0;
		livetime = 0.0;
	}

    int pixel;
	unsigned long triggers;
    unsigned long outputs;
    double trigger_livetime;
    unsigned long events_in_run;
    double input_count_rate; //input count rate
    double output_count_rate; //output count rate
    double deadtime;
    double realtime;
    double livetime;

    std::vector<DataType> data;
    std::vector<DataType> sca_rois;
};

//------------------------------------------------------------------
class ChannelData
{
public:
	ChannelData()
	{
		channel = 0;
	}
    int channel;
    PixelData pixel_data;
};

//------------------------------------------------------------------
class ModuleData
{
public:
	ModuleData()
	{
		module = 0;
	}
    int module;
    std::vector<ChannelData> channel_data;
};

//------------------------------------------------------------------
class Data
{
public:
	Data(){}
    std::vector<ModuleData> module_data;
};

//------------------------------------------------------------------
class DataBufferContainer
{
public:
    DataBufferContainer(int module, std::size_t buffer_length);
    virtual ~DataBufferContainer();
    DataType* base() const;
    int module() const;

private:
    int m_module;
    DataBufferPtr m_buffer;
};
typedef yat::SharedPtr<DataBufferContainer> DataBufferContainerPtr;
//------------------------------------------------------------------
//	class:	DataStore
//------------------------------------------------------------------
class DataStore : public yat4tango::DeviceTask
{
    friend class DataParser;
    friend class DataParserMapping;
    friend class DataParserMca;
public:
    ///ctor
    DataStore(Tango::DeviceImpl *dev);
    ///dtor
    virtual ~DataStore();
    void init(const std::string& board_type, int nb_modules, int nb_channels, int nb_channels_per_module, int nb_pixels, int nb_bins,  const std::string& acquisition_mode);
    //a light init when acquisition_mode is not changed :  used to reset state & exception flag only
    void reinit();
    void store_statistics(int module, int channel, int pixel, PixelData pix_data);
    void store_data(int module, int channel, int pixel, DataType* data, size_t length, bool is_last_mca = false);    
	void process_data(DataBufferContainerPtr map_buffer_ptr);
	void close_data();
    void abort_data();
    //stop_acquisition & abort_data
    void abort(std::string status);
    void subscribe(class Controller* observer);
    void set_nb_pixels(int nb_pixels);
    double get_timebase();  
	void set_timebase(double timebase);  
    
    ///getters
    int get_nb_modules();
    int get_nb_channels();
    int get_nb_channels_per_module();	
    int get_nb_pixels();
	int get_nb_bins();
    int get_nb_rois(int channel);
    std::string get_acquisition_mode();
    std::string get_board_type();
	long   get_current_pixel(int channel);	
	double get_trigger_livetime(int channel);	
    double get_realtime(int channel);
    double get_livetime(int channel);
    double get_deadtime(int channel);		
    double get_output_count_rate(int channel);
    double get_input_count_rate(int channel);
    unsigned long get_triggers(int channel);
    unsigned long get_outputs(int channel);
    unsigned long get_events_in_run(int channel);
    std::vector<DataType> get_channel_data(int channel);        
    std::vector<DataType> get_hroi_data(int channel);
    Tango::DevState get_state();
    std::string get_status();
	
	///return the cluster channel number from module number and channel number
	inline int TO_CHANNEL_CLUSTER(int module, int channel)
	{
		return (module * m_nb_channels_per_module + channel);
	}

	///return the std::pair<module number, channel number> from a cluster channel number
	inline std::pair<int, int> TO_MODULE_AND_CHANNEL(int channel)
	{
		return std::make_pair(((int) channel / (int) m_nb_channels_per_module), ((int) channel % (int) m_nb_channels_per_module));
	}	
protected:
    void process_message(yat::Message& msg) throw (Tango::DevFailed);      
private:
    ///inform controller (observer) about new datas . it has to update_data on (stream & view)
    void notify_data(int ichannel, bool update_stream = true, bool update_view = true);
    ///inform controller (observer) about end of datas . it has to close the stream
	void on_close_data();
    ///inform controller (observer) about anormal end of datas . it has to abort the stream
	void on_abort_data();    
    ///inform controller (observer) about exception occured. it has to stop acquisition & abort the stream
    void on_abort(std::string status);
    ///process the data's buffer in order to get statistics & datas from the acquired buffer
    void on_process_data(DataBufferContainerPtr map_buffer_ptr);	
    //build the data buffer parser UniquePtr
    void build_parser(const std::string& acq_mode);
    ///compute statistics
    double compute_realtime(double realtime);
    double compute_livetime(double livetime);
	double compute_deadtime(unsigned long outputs, unsigned long triggers, double livetime, double realtime);	
    double compute_deadtime(double ocr, double icr);	
    double compute_input_count_rate(unsigned long trigger_livetime, double livetime);
    double compute_output_count_rate(unsigned long outputs, double realtime);    
    ///
    void set_state(Tango::DevState state);
    void set_status(const std::string& status);    
    ///

    Tango::DevState m_state;
    std::stringstream m_status;
    yat::Mutex m_state_lock;
    yat::Mutex m_data_lock;
    Data m_data;
    std::string m_board_type;
    int m_nb_modules;
    int m_nb_channels;
	int m_nb_channels_per_module;
    int m_nb_pixels;
	int m_nb_bins;
    std::string m_acquisition_mode;
    double m_timebase;
    class Controller* m_controller;
    bool m_is_exception_stream_occured;
    /// Parser object
    yat::UniquePtr<DataParser> m_parser;
    /// Owner Device server object
    Tango::DeviceImpl* m_device;
};


//-------------------------------------------------------------
/// Functor object called when reset the object AcquisitionMapping
struct DataStoreTaskExiter
{
    /// unique operator() of this functor
    void operator()(yat4tango::DeviceTask* t) const
    {
        try
        {
            //- Automatically post the TASK_EXIT msg! U may never delete manually a DeviceTask !!!
            cout << "DataStore::TaskExiter ..." << endl;
            t->exit();
        }
        catch(...)
        {
        }
    }
};

} 

#endif //DATA_STORE_H
