/*************************************************************************
/*! 
 *  \file   SimulatorHelper.h
 *  \brief  class SimulatorHelper 
 *  \author Arafat Noureddine - SOLEIL (consultant MEDIANE SYSTEME)
 */
/*************************************************************************/

#ifndef SIMULATOR_HELPER_H
#define SIMULATOR_HELPER_H

//- TANGO
#include <tango.h>

#include <yat/time/Timer.h>
#include <yat/threading/Thread.h>
#include <yat/utils/StringTokenizer.h>
#include <yat/utils/String.h>
#include <math.h>
#include <DriverHelper.h>

////////////////////////////////////////////////////////
// HARDWARE CONSTANTS
////////////////////////////////////////////////////////
#define TAG_HEAD0 0x55aa 
#define TAG_HEAD1 0xaa55
#define TAG_DATA0 0x33cc
#define TAG_DATA1 0xcc33
const unsigned long BUFFER_HEADER_SIZE = 256;
const unsigned long PIXEL_HEADER_SIZE = 256;
const unsigned long PIXEL_HEADER_SIZE_SCA = 64;
////////////////////////////////////////////////////////

////////////////////////////////////////////////////////

//fix (default) nb modules of the simulator (could be modified by property BoardType)
const int NUMERIC_NB_MODULES = 1;
//fix (default) nb modules of the simulator (could be modified by property BoardType)
const int NUMERIC_NB_CHANNELS = 4;
//fix (default) nb modules per module of the simulator (could be modified by property BoardType)
const int NUMERIC_NB_CHANNELS_PER_MODULE = 4;
//fix (default) acquisition mapping clock of the simulator (could be modified by property BoardType)
const unsigned long NUMERIC_MAPPING_CLOCK_MS = 1; //ms
//fix (default) load configuration delay of the simulator (could be modified by property BoardType)
const int NUMERIC_LOAD_CONIGURATION_DELAY_MS = 5000;//in ms

//fix other parameters
const int NUMERIC_NB_BINS = 2048;
const int NUMERIC_NB_SCA_MAX = 64;
const int NUMERIC_MCA_LENGTH = NUMERIC_NB_BINS;
const int NUMERIC_BUFFER_LENGTH = BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 4 * NUMERIC_NB_BINS;
const int NUMERIC_BUFFER_LENGTH_SCA = BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE_SCA + 4 * NUMERIC_NB_SCA_MAX;
const double NUMERIC_DYNAMIC_RANGE = 3.0;
const double NUMERIC_PEAKING_TIME = 2.0;
const double NUMERIC_PRESET_VALUE = 4.0; //in s
const double NUMERIC_NUMBER_MCA_CHANNELS = NUMERIC_NB_BINS; //nb_bins
const double NUMERIC_REALTIME = 2.2;
const double NUMERIC_LIVETIME = 3.3;
const double NUMERIC_TRIGGER_LIVETIME = 1.1;
const double NUMERIC_ICR = 5.5;
const double NUMERIC_OCR = 4.4;
const unsigned long NUMERIC_EVENTS_IN_RUN = 6;
const double NUMERIC_NUM_MAP_PIXELS = 200;
const double NUMERIC_NUM_MAP_PIXELS_PER_BUFFER = 100;
const std::string NUMERIC_PIXEL_ADVANCE_MODE = "GATE";


const std::string SIMULATOR_TYPE = "SIMULATOR";
////////////////////////////////////////////////////////
const double Pi = 3.141592654f;
inline double DegToRad(double x)
{
    return x / 180 * Pi;
}

inline double RadToDeg(double x)
{
    return x / Pi * 180;
}


////////////////////////////////////////////////////////

namespace XiaDxp_ns
{
class SimulatorHelper : public DriverHelper
{
public:

    ////////////////////////////////////////////////////////////////////////
    SimulatorHelper(Tango::DeviceImpl *dev, const std::string & board_type) : DriverHelper(dev, board_type)
    {
        INFO_STREAM << "SimulatorHelper::SimulatorHelper()- [BEGIN]" << endl;
        m_is_library_loaded = true;
        std::vector<std::string> vec_lines;
        yat::StringUtil::split(board_type, ';', &vec_lines, true);

        m_board_type                    = (vec_lines.size()>0)?vec_lines.at(0):"SIMULATOR";
        m_nb_modules                    = (vec_lines.size()>1)?yat::StringUtil::to_num<int>(vec_lines.at(1)):NUMERIC_NB_MODULES;
        m_nb_channels                   = (vec_lines.size()>2)?yat::StringUtil::to_num<int>(vec_lines.at(2)):NUMERIC_NB_CHANNELS;
        m_nb_channels_per_module        = (vec_lines.size()>3)?yat::StringUtil::to_num<int>(vec_lines.at(3)):NUMERIC_NB_CHANNELS_PER_MODULE;		
        m_mapping_clock_ms              = (vec_lines.size()>4)?yat::StringUtil::to_num<int>(vec_lines.at(4)):NUMERIC_MAPPING_CLOCK_MS;
        m_load_configuration_delay_ms   = (vec_lines.size()>5)?yat::StringUtil::to_num<int>(vec_lines.at(5)):NUMERIC_LOAD_CONIGURATION_DELAY_MS;

        INFO_STREAM << "\t- Board type  : " << m_board_type <<endl;
        INFO_STREAM << "\t- Nb. Module  : " << m_nb_modules <<endl;
        INFO_STREAM << "\t- Nb. Channels  : " << m_nb_channels <<endl;
        INFO_STREAM << "\t- Nb. Channels Per Module  : " << m_nb_channels_per_module <<endl;		
        INFO_STREAM << "\t- Nb. Bins  : " << NUMERIC_NB_BINS <<endl;
        INFO_STREAM << "\t- Mapping clock : " << m_mapping_clock_ms <<" (ms)"<<endl;
        INFO_STREAM << "\t- Load Configuration delay : " << m_load_configuration_delay_ms <<" (ms)"<<endl;


        m_mode = "MCA";//default mode
        m_status = "Board Type : " + m_board_type;
        m_state = INITIALIZATION_SUCCESSFUL;
        m_is_running = false;
        m_numero_pixel.resize(m_nb_modules);
        for (unsigned i = 0; i < m_nb_modules; ++i)
        {
            m_numero_pixel[i] = 0;
        }

        m_map_rois.clear();
        //prepare boards parameters values
        m_map_parameters.clear();
        m_map_parameters["nb_modules"] = yat::StringUtil::to_string<int>(m_nb_modules);
        m_map_parameters["nb_channels"] = yat::StringUtil::to_string<int>(m_nb_channels);
        m_map_parameters["nb_channels_per_module"] = yat::StringUtil::to_string<int>(m_nb_channels_per_module);		
        m_map_parameters["mca_length"] = yat::StringUtil::to_string<double>(NUMERIC_MCA_LENGTH);
        m_map_parameters["buffer_len"] = yat::StringUtil::to_string<double>(NUMERIC_BUFFER_LENGTH);//default is MCA
        m_map_parameters["number_mca_channels"] = yat::StringUtil::to_string<double>(NUMERIC_NUMBER_MCA_CHANNELS);
        m_map_parameters["dynamic_range"] = yat::StringUtil::to_string<double>(NUMERIC_DYNAMIC_RANGE);
        m_map_parameters["peaking_time"] = yat::StringUtil::to_string<double>(NUMERIC_PEAKING_TIME);
        m_map_parameters["preset_value"] = yat::StringUtil::to_string<double>(NUMERIC_PRESET_VALUE);
        m_map_parameters["preset_type"] = yat::StringUtil::to_string<double>(XIA_PRESET_FIXED_REAL);
        m_map_parameters["num_map_pixels"] = yat::StringUtil::to_string<double>(NUMERIC_NUM_MAP_PIXELS);
        m_map_parameters["num_map_pixels_per_buffer"] = yat::StringUtil::to_string<double>(NUMERIC_NUM_MAP_PIXELS_PER_BUFFER);
        m_map_parameters["pixel_advance_mode"] = std::string(NUMERIC_PIXEL_ADVANCE_MODE);
        m_map_parameters["realtime"] = yat::StringUtil::to_string<double>(NUMERIC_PRESET_VALUE+0.2);
        m_map_parameters["livetime"] = yat::StringUtil::to_string<double>(NUMERIC_PRESET_VALUE+0.3);
        m_map_parameters["trigger_livetime"] = yat::StringUtil::to_string<double>(NUMERIC_PRESET_VALUE+0.1);
        m_map_parameters["input_count_rate"] = yat::StringUtil::to_string<double>(NUMERIC_ICR);
        m_map_parameters["output_count_rate"] = yat::StringUtil::to_string<double>(NUMERIC_OCR);
        m_map_parameters["events_in_run"] = yat::StringUtil::to_string<unsigned long>(NUMERIC_EVENTS_IN_RUN);
        //
        m_timeout_preset = yat::Timeout(NUMERIC_PRESET_VALUE, yat::Timeout::TMO_UNIT_SEC); //in s
        m_timeout_load = yat::Timeout(m_load_configuration_delay_ms, yat::Timeout::TMO_UNIT_MSEC); //in s
        INFO_STREAM << "SimulatorHelper::SimulatorHelper()- [END]" << endl;
    }

    ////////////////////////////////////////////////////////////////////////
    virtual ~SimulatorHelper()
    {
        INFO_STREAM << "SimulatorHelper::~SimulatorHelper()- [BEGIN]" << endl;
        INFO_STREAM << "SimulatorHelper::~SimulatorHelper()- [END]" << endl;
    }

    ////////////////////////////////////////////////////////////////////////
    void load_config_file(const std::string& config_file,const std::string& mode)
    {
        INFO_STREAM << "Load config file '" << config_file << "' ..." << endl;
        m_mode = mode;
        if(m_mode == "MCA")
        {
            INFO_STREAM << "------------------------------------" << endl;            
            INFO_STREAM << "Simulated data in MCA will be :" << endl;
            INFO_STREAM << "f(x)= x�\tfor channel00" << endl;
            INFO_STREAM << "f(x)= x\tfor channel01" << endl;
            INFO_STREAM << "f(x)= x�\tfor channel02" << endl;
            INFO_STREAM << "f(x)= x\tfor channel03" << endl;
            INFO_STREAM << "------------------------------------" << endl;            
            m_map_parameters["buffer_len"] = yat::StringUtil::to_string<double>(NUMERIC_BUFFER_LENGTH);
        }
        else if(m_mode == "MAPPING"||m_mode == "MAPPING_FULL")
        {
            INFO_STREAM << "------------------------------------" << endl;
            INFO_STREAM << "Simulated data in MAPPING_FULL will be :" << endl;
            INFO_STREAM << "f(x)= x\t\t\tfor channel00" << endl;
            INFO_STREAM << "f(x)= sqrt(x)\t\tfor channel01" << endl;
            INFO_STREAM << "f(x)= sin�(x)*x\t\tfor channel02" << endl;
            INFO_STREAM << "f(x)= abs(sin(x)/x)*100000\tfor channel03" << endl;
            INFO_STREAM << "------------------------------------" << endl;
            m_map_parameters["buffer_len"] = yat::StringUtil::to_string<double>(NUMERIC_BUFFER_LENGTH);
        }
        else if(m_mode == "MAPPING_SCA")
        {
            INFO_STREAM << "------------------------------------" << endl;
            INFO_STREAM << "Simulated data in MAPPING_SCA will be :" << endl;
            INFO_STREAM << "f(x)= x\t\tfor channel00" << endl;
            INFO_STREAM << "f(x)= sqrt(x)\t\tfor channel01" << endl;
            INFO_STREAM << "f(x)= sin�(x)*x\tfor channel02" << endl;
            INFO_STREAM << "f(x)= abs(sin(x)/x)*100000\tfor channel03" << endl;
            INFO_STREAM << "------------------------------------" << endl;            
            m_map_parameters["buffer_len"] = yat::StringUtil::to_string<double>(NUMERIC_BUFFER_LENGTH_SCA);
        }
        m_timeout_load.restart();
        while(!m_timeout_load.expired())
            yat::Thread::sleep(1000);//1s
    }

    ////////////////////////////////////////////////////////////////////////
    void save_config_file(const std::string& config_file)
    {
        INFO_STREAM << "Save config file '" << config_file << "' ..." << endl;
        return;
    }

    ////////////////////////////////////////////////////////////////////////
    void start_acquisition(short accumulate)
    {
        m_is_running = true;
        for (unsigned i = 0; i < m_nb_modules; ++i)
        {
            m_numero_pixel[i] = 0;
        }
        m_timeout_preset.restart();
        return;
    }

    ////////////////////////////////////////////////////////////////////////
    void stop_acquisition()
    {
        m_is_running = false;
        return;
    }

    ////////////////////////////////////////////////////////////////////////
    int get_nb_modules()
    {
        return yat::StringUtil::to_num<int>(m_map_parameters["nb_modules"]);
    }

    ////////////////////////////////////////////////////////////////////////
    int get_nb_channels()
    {
        return yat::StringUtil::to_num<int>(m_map_parameters["nb_channels"]);
    }

	////////////////////////////////////////////////////////////////////////
	virtual int get_nb_channels_per_module()
	{
		return yat::StringUtil::to_num<int>(m_map_parameters["nb_channels_per_module"]);
	}

	
    ////////////////////////////////////////////////////////////////////////    
    int get_nb_bins(int channel)
    {
        return yat::StringUtil::to_num<int>(m_map_parameters["number_mca_channels"]);
    }

    ////////////////////////////////////////////////////////////////////////    
    void set_nb_bins(int channel, int value)
    {
        m_map_parameters["number_mca_channels"] = yat::StringUtil::to_string<double>(value);
    }

    ////////////////////////////////////////////////////////////////////////
    double get_preset_type(int channel)
    {
        return yat::StringUtil::to_num<double>(m_map_parameters["preset_type"]);
    }

    ////////////////////////////////////////////////////////////////////////
    void set_preset_type(int channel, double value)
    {
        m_map_parameters["preset_type"] = yat::StringUtil::to_string<double>(value);
    }

    ////////////////////////////////////////////////////////////////////////
    double get_preset_value(int channel)
    {
        return yat::StringUtil::to_num<double>(m_map_parameters["preset_value"]);
    }

    ////////////////////////////////////////////////////////////////////////
    void set_preset_value(int channel, double value)
    {
        m_map_parameters["preset_value"] = yat::StringUtil::to_string<double>(value);
        m_timeout_preset.set_value(value);
    }
    ////////////////////////////////////////////////////////////////////////
    double get_peaking_time(int channel)
    {
        return yat::StringUtil::to_num<double>(m_map_parameters["peaking_time"]);
    }

    ////////////////////////////////////////////////////////////////////////
    void set_peaking_time(int channel, double value)
    {
        m_map_parameters["peaking_time"] = yat::StringUtil::to_string<double>(value);
    }

    ////////////////////////////////////////////////////////////////////////
    double get_dynamic_range(int channel)
    {
        return yat::StringUtil::to_num<double>(m_map_parameters["dynamic_range"]);
    }
    ////////////////////////////////////////////////////////////////////////
    void set_dynamic_range(int channel, double value)
    {
        m_map_parameters["dynamic_range"] = yat::StringUtil::to_string<double>(value);
    }

    ////////////////////////////////////////////////////////////////////////    
    int get_num_map_pixels(int channel)
    {
        return(int) (yat::StringUtil::to_num<double>(m_map_parameters["num_map_pixels"]));
    }
    ////////////////////////////////////////////////////////////////////////
    void set_num_map_pixels(int channel, double value)
    {
        m_map_parameters["num_map_pixels"] = yat::StringUtil::to_string<double>(value);
    }

    ////////////////////////////////////////////////////////////////////////
    int get_num_map_pixels_per_buffer(int channel)
    {
        return(int) (yat::StringUtil::to_num<double>(m_map_parameters["num_map_pixels_per_buffer"]));
    }

    ////////////////////////////////////////////////////////////////////////
    void set_num_map_pixels_per_buffer(int channel, double value)
    {
        m_map_parameters["num_map_pixels_per_buffer"] = yat::StringUtil::to_string<double>(value);
    }

    ////////////////////////////////////////////////////////////////////////
    double get_number_of_scas(int channel)
    {
        return yat::StringUtil::to_num<double>(m_map_parameters["number_of_scas"]);
    }

    ////////////////////////////////////////////////////////////////////////
    void set_number_of_scas(int channel, double value)
    {
        m_map_parameters["number_of_scas"] = yat::StringUtil::to_string<double>(value);
    }

    ////////////////////////////////////////////////////////////////////////
    State get_state()
    {
        return m_state;
    }

    ////////////////////////////////////////////////////////////////////////
    std::string get_status()
    {
        return m_status;
    };

    ////////////////////////////////////////////////////////////////////////
    Tango::DevLong get_mca_length(int channel)
    {
        return yat::StringUtil::to_num<long>(m_map_parameters["mca_length"]);
    }

    ////////////////////////////////////////////////////////////////////////
    void get_mca_data(int channel, int number_of_bins, std::vector<Tango::DevULong>& result)
    {
        result.resize(number_of_bins);
        if(channel % 2)
        {
            //simulate f(x)= x for channel odd
            for(size_t i = 0; i < result.size(); i++)
                result.at(i) = (Tango::DevULong)(i);
        }
        else
        {
            //simulate f(x)=x� for channel even
            for(size_t i = 0; i < result.size(); i++)
                result.at(i) = (Tango::DevULong)(pow((double)i,2)/NUMERIC_NB_BINS);
        }

        /*if a preset_value (s) is expired, then change state to standby*/
        if(m_timeout_preset.expired())
            m_is_running = false;

        return;
    }

    ////////////////////////////////////////////////////////////////////////
    void get_run_data(int channel, const char* name, void* value)
    {
        if(std::string(name) == "realtime")
            *(static_cast<double*> (value)) = yat::StringUtil::to_num<double>(m_map_parameters["realtime"]);
        else if(std::string(name) == "livetime")
            *(static_cast<double*> (value)) = yat::StringUtil::to_num<double>(m_map_parameters["livetime"]);
        else if(std::string(name) == "trigger_livetime")
            *(static_cast<double*> (value)) = yat::StringUtil::to_num<double>(m_map_parameters["trigger_livetime"]);
        else if(std::string(name) == "input_count_rate")
            *(static_cast<double*> (value)) = yat::StringUtil::to_num<double>(m_map_parameters["input_count_rate"]);
        else if(std::string(name) == "output_count_rate")
            *(static_cast<double*> (value)) = yat::StringUtil::to_num<double>(m_map_parameters["output_count_rate"]);
        else if(std::string(name) == "events_in_run")
            *(static_cast<unsigned long*> (value)) = yat::StringUtil::to_num<unsigned long>(m_map_parameters["events_in_run"]);
        return;
    }

    ////////////////////////////////////////////////////////////////////////
    int get_nb_rois(int channel)
    {
        std::vector<double> bounds = m_map_rois[channel];
        int roi_count = bounds.size() / 2;
        return roi_count;
    }

    ////////////////////////////////////////////////////////////////////////
    void set_nb_rois(int channel, int nb_rois)
    {
        if(nb_rois == 0)
            m_map_rois[channel].clear();
        else
            m_map_rois[channel].resize(nb_rois * 2);
    }

    ////////////////////////////////////////////////////////////////////////
    void get_roi_bounds(int channel, int roi_num, double& low, double& high)
    {
        std::vector<double> bounds = m_map_rois[channel];
        low = bounds.at(2 * roi_num);
        high = bounds.at(2 * roi_num + 1);
    }

    ////////////////////////////////////////////////////////////////////////    
    void set_roi_bounds(int channel, int roi_num, double low, double high)
    {        
        std::vector<double> bounds = m_map_rois[channel];
        //if a new roi_num, then resize vector in order to insert a new low/high bounds
        if((size_t) roi_num >= bounds.size() / 2)
        {
            bounds.resize(roi_num * 2);
        }

        bounds[2 * roi_num] = low;
        bounds[2 * roi_num + 1] = high;
        m_map_rois[channel] = bounds;
        for(std::map<int, std::vector<double>>::const_iterator it = m_map_rois.begin(); it != m_map_rois.end(); ++it)
        {
            std::stringstream ss;
            ss << "channel = " << it->first;
            for(size_t i = 0; i < it->second.size() / 2; i++)
            {
                ss << ", roi" << i << "_low  = " << it->second.at(2 * i);
                ss << ", roi" << i << "_high = " << it->second.at(2 * i + 1);
            }
            DEBUG_STREAM << ss.str() << endl;
        }
    }

    ////////////////////////////////////////////////////////////////////////
    double get_roi_data(int channel, int roi_num)
    {
        /*roi_data is  equal to high-low+1*/
        std::vector<double> bounds = m_map_rois[channel];
        return(bounds[2 * roi_num + 1] - bounds[2 * roi_num]);
    }

    ////////////////////////////////////////////////////////////////////////
    bool is_running()
    {
        //MCA mode : managed in get_mca_data() :
        //a timer object is initialized with the preset_value() fixed by user
        //if the timer expired, m_is_running = false
        //--------------------------------------------------------------------
        //MAPPING mode : managed in buffer_done() :
        //if (m_numero_pixel == nbPixels) i.e all pixels are acquired, then m_is_running = false
        return m_is_running;
    }

    ////////////////////////////////////////////////////////////////////////
    std::string get_pixel_advance_mode()
    {
        return m_map_parameters["pixel_advance_mode"];
    }

    ////////////////////////////////////////////////////////////////////////
    void set_pixel_advance_mode(const std::string& mode, double ticks_per_pixel = 0.0)
    {
        m_map_parameters["pixel_advance_mode"] = mode;
    }

    ////////////////////////////////////////////////////////////////////////
    void mapping_pixel_next()
    {
        return;
    }

    ////////////////////////////////////////////////////////////////////////
    bool is_buffer_overrun(int module)
    {
        /*always in order to never throw exception overrun inthe simulator*/
        return false;
    }

    ////////////////////////////////////////////////////////////////////////
    bool is_buffer_full(int module, Buffer buffer_num)
    {
        /*always in order to free run the simulator*/
        return true;
    }

    ////////////////////////////////////////////////////////////////////////
    unsigned long get_buffer_length(int module)
    {
        return yat::StringUtil::to_num<int>(m_map_parameters["buffer_len"])*4;
    }

    ////////////////////////////////////////////////////////////////////////
    unsigned long get_current_pixel()
    {
        return m_numero_pixel[m_nb_modules-1];
    }

    ////////////////////////////////////////////////////////////////////////
    void get_buffer(int module, Buffer buffer_num, unsigned long* buffer)
    {

        if(m_mode == "MAPPING" || m_mode == "MAPPING_FULL" ||m_mode == "MCA")
        {
            for(unsigned i = 0; i < NUMERIC_BUFFER_LENGTH; i++)
            {
                switch(i)
                {
                        //HEADER
                    case 0:buffer[i] = TAG_HEAD0;
                        break;
                    case 1:buffer[i] = TAG_HEAD1;
                        break;
                    case 2:buffer[i] = BUFFER_HEADER_SIZE; //buffer header size
                        break;
                    case 3:buffer[i] = 1; //mapping mode
                        break;
                    case 4:buffer[i] = 1; //run number
                        break;
                    case 5:buffer[i] = 0; //seq buffer number
                        break;
                    case 6:buffer[i] = 0; //seq buffer number
                        break;
                    case 7:buffer[i] = 0; //buffer A
                        break;
                    case 8:buffer[i] = 1; //nb of pixels in buffer
                        break;
                    case 9:buffer[i] = m_numero_pixel[module]; //starting pixel in buffer LSB
                        break;
                    case 10:buffer[i] = 0; //starting pixel in buffer MSB
                        break;
                    case 31:buffer[i] = 0; //reserved must be set to 0
                        break;
                        //DATA
                    case BUFFER_HEADER_SIZE + 0: buffer[i] = TAG_DATA0;                         //TAG WORD : 0x33CC
                        break;
                    case BUFFER_HEADER_SIZE + 1: buffer[i] = TAG_DATA1;                         //TAG WORD : 0xCC33
                        break;
                    case BUFFER_HEADER_SIZE + 4: buffer[i] = m_numero_pixel[module];            //pixel number LSB
                        break;
                    case BUFFER_HEADER_SIZE + 5: buffer[i] = 0;                                 //pixel number MSB
                        break;
                    case BUFFER_HEADER_SIZE + 8: buffer[i] = (unsigned long)NUMERIC_NB_BINS;    //channel 0 size
                        break;
                    case BUFFER_HEADER_SIZE + 9: buffer[i] = NUMERIC_NB_BINS;                   //channel 1 size
                        break;
                    case BUFFER_HEADER_SIZE + 10: buffer[i] = NUMERIC_NB_BINS;                  //channel 2 size
                        break;
                    case BUFFER_HEADER_SIZE + 11: buffer[i] = NUMERIC_NB_BINS;                  //channel 3 size
                        break;
                        //statistics channel 0
                    case BUFFER_HEADER_SIZE + 32: buffer[i] = 32000000;                         //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 33: buffer[i] = 0;                                //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 34: buffer[i] = 34000000;                         //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 35: buffer[i] = 0;                                //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 36: buffer[i] = 36000000;                         //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 37: buffer[i] = 0;                                //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 38: buffer[i] = 38000000;                         //outputs
                        break;
                    case BUFFER_HEADER_SIZE + 39: buffer[i] = 0;                                //outputs
                        break;
                        //statistics channel 1    
                    case BUFFER_HEADER_SIZE + 40: buffer[i] = (unsigned long)NUMERIC_REALTIME;  //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 41: buffer[i] = 0;                                //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 42: buffer[i] = (unsigned long)NUMERIC_LIVETIME;  //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 43: buffer[i] = 0;                                //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 44: buffer[i] = (unsigned long)NUMERIC_ICR;       //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 45: buffer[i] = 0;                                //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 46: buffer[i] = (unsigned long)NUMERIC_OCR;       //outputs
                        break;
                    case BUFFER_HEADER_SIZE + 47: buffer[i] = 0;                                //outputs
                        break;
                        //statistics channel 2    
                    case BUFFER_HEADER_SIZE + 48: buffer[i] = (unsigned long)NUMERIC_REALTIME;  //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 49: buffer[i] = 0;                                //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 50: buffer[i] = (unsigned long)NUMERIC_LIVETIME;  //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 51: buffer[i] = 0;                                //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 52: buffer[i] = (unsigned long)NUMERIC_ICR;       //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 53: buffer[i] = 0;                                //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 54: buffer[i] = (unsigned long)NUMERIC_OCR;       //outputs
                        break;
                    case BUFFER_HEADER_SIZE + 55: buffer[i] = 0;                                //outputs
                        break;
                        //statistics channel 3    
                    case BUFFER_HEADER_SIZE + 56: buffer[i] = (unsigned long)NUMERIC_REALTIME;  //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 57: buffer[i] = 0;                                //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 58: buffer[i] = (unsigned long)NUMERIC_LIVETIME;  //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 59: buffer[i] = 0;                                //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 60: buffer[i] = (unsigned long)NUMERIC_ICR;       //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 61: buffer[i] = 0;                                //trigers
                        break;
                    case BUFFER_HEADER_SIZE + 62: buffer[i] = (unsigned long)NUMERIC_OCR;       //outputs
                        break;
                    case BUFFER_HEADER_SIZE + 63: buffer[i] = 0;                                //outputs
                        break;

                        //channel0 data (simulate x)
                    case BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 0:
                        for(unsigned j = 0; j < NUMERIC_NB_BINS; j++)
                            buffer[BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 0 * NUMERIC_NB_BINS + j] = (unsigned long) (j);
                        break;
                        //channel1 data (simulate sqrt(x))
                    case BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 1*NUMERIC_NB_BINS:
                        for(unsigned j = 0; j < NUMERIC_NB_BINS; j++)
                            buffer[BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 1 * NUMERIC_NB_BINS + j] = (unsigned long) (sqrt((double) j));
                        break;
                        //channel2 data (simulate sin²(x)*x)
                    case BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 2*NUMERIC_NB_BINS:
                        for(unsigned j = 0; j < NUMERIC_NB_BINS; j++)
                            buffer[BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 2 * NUMERIC_NB_BINS + j] = (unsigned long) (pow(sin(DegToRad(j)),2)*j);
                        break;
                        //channel3 data (simulate 100000*abs(sin(x)/x))
                    case BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 3*NUMERIC_NB_BINS:
                        for(unsigned j = 0; j < NUMERIC_NB_BINS; j++)
                            buffer[BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE + 3 * NUMERIC_NB_BINS + j] = (unsigned long) (100000*fabs(sin(DegToRad(j)))/((double)j+1));
                        break;
                    default:
                        break;
                }
            }
        }
        else//MAPPING_SCA
        {
            unsigned nb_roi_0 = get_nb_rois(module * 4 + 0);
            unsigned nb_roi_1 = get_nb_rois(module * 4 + 1);
            unsigned nb_roi_2 = get_nb_rois(module * 4 + 2);
            unsigned nb_roi_3 = get_nb_rois(module * 4 + 3);
            for(unsigned i = 0; i < NUMERIC_BUFFER_LENGTH_SCA; i++)
            {
                switch(i)
                {
                        //HEADER
                    case 0:buffer[i] = TAG_HEAD0;
                        break;
                    case 1:buffer[i] = TAG_HEAD1;
                        break;
                    case 2:buffer[i] = BUFFER_HEADER_SIZE; //buffer header size
                        break;
                    case 3:buffer[i] = 1; //mapping mode
                        break;
                    case 4:buffer[i] = 1; //run number
                        break;
                    case 5:buffer[i] = 0; //seq buffer number
                        break;
                    case 6:buffer[i] = 0; //seq buffer number
                        break;
                    case 7:buffer[i] = 0; //buffer A
                        break;
                    case 8:buffer[i] = 1; //nb of pixels in buffer
                        break;
                    case 9:buffer[i] = m_numero_pixel[module]; //starting pixel in buffer LSB
                        break;
                    case 10:buffer[i] = 0; //starting pixel in buffer MSB
                        break;
                    case 31:buffer[i] = 0; //reserved must be set to 0
                        break;
                        //DATA
                    case BUFFER_HEADER_SIZE + 0: buffer[i] = TAG_DATA0; //TAG WORD : 0x33CC
                        break;
                    case BUFFER_HEADER_SIZE + 1: buffer[i] = TAG_DATA1; //TAG WORD : 0xCC33
                        break;
                    case BUFFER_HEADER_SIZE + 4: buffer[i] = m_numero_pixel[module]; //pixel number LSB
                        break;
                    case BUFFER_HEADER_SIZE + 5: buffer[i] = 0; //pixel number MSB
                        break;
                    case BUFFER_HEADER_SIZE + 8: buffer[i] = nb_roi_0; //channel 0 NB ROIS
                        break;
                    case BUFFER_HEADER_SIZE + 9: buffer[i] = nb_roi_1; //channel 1 NB ROIS
                        break;
                    case BUFFER_HEADER_SIZE + 10: buffer[i]= nb_roi_2; //channel 2 NB ROIS
                        break;
                    case BUFFER_HEADER_SIZE + 11: buffer[i]= nb_roi_3; //channel 3 NB ROIS
                        break;
                        //statistics channel 0
                    case BUFFER_HEADER_SIZE + 32: buffer[i] = (unsigned long)NUMERIC_REALTIME;  //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 33: buffer[i] = 0;                                //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 34: buffer[i] = (unsigned long)NUMERIC_LIVETIME;  //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 35: buffer[i] = 0;                                //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 36: buffer[i] = (unsigned long)NUMERIC_ICR;       //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 37: buffer[i] = 0;                                //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 38: buffer[i] = (unsigned long)NUMERIC_OCR;       //outputs
                        break;
                    case BUFFER_HEADER_SIZE + 39: buffer[i] = 0;                                //outputs
                        break;
                        //statistics channel 1    
                    case BUFFER_HEADER_SIZE + 40: buffer[i] = (unsigned long)NUMERIC_REALTIME;  //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 41: buffer[i] = 0;                                //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 42: buffer[i] = (unsigned long)NUMERIC_LIVETIME;  //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 43: buffer[i] = 0;                                //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 44: buffer[i] = (unsigned long)NUMERIC_ICR;       //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 45: buffer[i] = 0;                                //trigers
                        break;
                    case BUFFER_HEADER_SIZE + 46: buffer[i] = (unsigned long)NUMERIC_OCR;       //outputs
                        break;
                    case BUFFER_HEADER_SIZE + 47: buffer[i] = 0;                                //outputs
                        break;
                        //statistics channel 2    
                    case BUFFER_HEADER_SIZE + 48: buffer[i] = (unsigned long)NUMERIC_REALTIME;  //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 49: buffer[i] = 0;                                //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 50: buffer[i] = (unsigned long)NUMERIC_LIVETIME;  //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 51: buffer[i] = 0;                                //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 52: buffer[i] = (unsigned long)NUMERIC_ICR;       //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 53: buffer[i] = 0;                                //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 54: buffer[i] = (unsigned long)NUMERIC_OCR;       //outputs
                        break;
                    case BUFFER_HEADER_SIZE + 55: buffer[i] = 0;                                //outputs
                        break;
                        //statistics channel 3    
                    case BUFFER_HEADER_SIZE + 56: buffer[i] = (unsigned long)NUMERIC_REALTIME;  //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 57: buffer[i] = 0;                                //realtime
                        break;
                    case BUFFER_HEADER_SIZE + 58: buffer[i] = (unsigned long)NUMERIC_LIVETIME;  //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 59: buffer[i] = 0;                                //livetime
                        break;
                    case BUFFER_HEADER_SIZE + 60: buffer[i] = (unsigned long)NUMERIC_ICR;       //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 61: buffer[i] = 0;                                //triggers
                        break;
                    case BUFFER_HEADER_SIZE + 62: buffer[i] = (unsigned long)NUMERIC_OCR;       //outputs
                        break;
                    case BUFFER_HEADER_SIZE + 63: buffer[i] = 0;                                //outputs
                        break;
                        //roi data
                    case BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE_SCA + 0:
                        //channel0 data (simulate x)
                        for(unsigned j = 0; j < nb_roi_0; j++)
                            buffer[BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE_SCA + j] = (unsigned long) (m_numero_pixel[module]);
                        //channel1 data (simulate sqrt(x))
                        for(unsigned j = 0; j < nb_roi_1; j++)
                            buffer[BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE_SCA + nb_roi_0 + j] = (unsigned long) (sqrt((double) m_numero_pixel[module]));
                        //channel2 data (simulate sin²(x)*x)
                        for(unsigned j = 0; j < nb_roi_2; j++)
                            buffer[BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE_SCA + nb_roi_0 + nb_roi_1 + j] = (unsigned long) (pow(sin(DegToRad(m_numero_pixel[module])),2)*m_numero_pixel[module]);
                        //channel3 data (simulate 100000*abs(sin(x)/x))
                        for(unsigned j = 0; j < nb_roi_3; j++)
                            buffer[BUFFER_HEADER_SIZE + PIXEL_HEADER_SIZE_SCA + nb_roi_0 + nb_roi_1 + nb_roi_2 + j] = (unsigned long) (100000*fabs(sin(DegToRad(m_numero_pixel[module])))/(m_numero_pixel[module]+1));
                        break;

                    default:
                        break;
                }
            }
        }

        //in ms in order to simulate  a duration of acquisition
        yat::Thread::sleep(m_mapping_clock_ms);
        return;
    }

    ////////////////////////////////////////////////////////////////////////
    void buffer_done(int module, Buffer buffer_num)
    {
        /*if all pixels are acquired, then change state to standby*/
        if(m_numero_pixel[module] == get_num_map_pixels(0))
            m_is_running = false;
        m_numero_pixel[module]++;
        return;
    }

    ////////////////////////////////////////////////////////////////////////
    void apply_all_board_operation(const char* operation)
    {
        return;
    }

    ////////////////////////////////////////////////////////////////////////        
    std::string get_version()
    {
        return "3.0.0 (Simulator)";
    }
    ////////////////////////////////////////////////////////////////////////    
    
    void load_library()
    {
        INFO_STREAM << "SimulatorHelper::load_library()- NA" << endl;   
    }
    ////////////////////////////////////////////////////////////////////////        
    void free_library()
    {
        INFO_STREAM << "SimulatorHelper::free_library()- NA" << endl;   
    }
    ////////////////////////////////////////////////////////////////////////        
////////////////////////////////////////////////////////////////////////        
    bool is_library_loaded()
    {
        INFO_STREAM << "SimulatorHelper::is_library_loaded()- NA" << endl;   
        return m_is_library_loaded;        
    }
    ////////////////////////////////////////////////////////////////////////            
private:
    bool m_is_running;
    std::map<int, std::vector<double>> m_map_rois;
    yat::Timeout m_timeout_preset;
    yat::Timeout m_timeout_load;
    std::vector<int> m_numero_pixel;
    std::map<std::string, std::string> m_map_parameters;
    std::string m_mode;
    std::string m_board_type;
    unsigned m_nb_modules;
    unsigned m_nb_channels;
    unsigned m_nb_channels_per_module;	
    unsigned m_mapping_clock_ms;
    unsigned m_load_configuration_delay_ms;
};

} // namespace XiaDxp_ns


#endif

