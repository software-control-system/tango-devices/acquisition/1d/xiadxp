//=============================================================================
//
// file :        DataParserMca.cpp
//
// description : 
//
// project :	XiaDxp Project
//
// $Author: noureddine $
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//=============================================================================

#include <tango.h>
#include "Controller.h"
#include "DataStore.h"
#include "DataParser.h"
#include "DataParserMca.h"


namespace XiaDxp_ns
{
//----------------------------------------------------------------------------------------------------------------------
//- ctor
//----------------------------------------------------------------------------------------------------------------------
DataParserMca::DataParserMca(Tango::DeviceImpl *dev,DataStore* store)
: DataParser(dev, store)
{
    INFO_STREAM << "DataParserMca::DataParserMca() - [BEGIN]" << endl;
    INFO_STREAM << "DataParserMca::DataParserMca() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- dtor
//----------------------------------------------------------------------------------------------------------------------
DataParserMca::~DataParserMca()
{
    INFO_STREAM << "DataParserMca::~DataParserMca() - [BEGIN]" << endl;
    INFO_STREAM << "DataParserMca::~DataParserMca() - [END]" << endl;
}

//----------------------------------------------------------------------------------------------------------------------
//- Parse entire buffer acquisition
//----------------------------------------------------------------------------------------------------------------------
void DataParserMca::parse_buffer(DataBufferContainerPtr map_buffer)
{
	DEBUG_STREAM << "DataParserMca::parse_buffer() - [BEGIN]" << std::endl;
	INFO_STREAM << "\t- parse the MCA buffer : Nothing to do !" << endl;
    //Nothing to do
	DEBUG_STREAM << "DataParserMca::parse_buffer() - [END]" << std::endl;
}

} // namespace 
