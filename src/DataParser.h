/*************************************************************************
/*! 
 *  \file   DataStore.h
 *  \brief  class DataStore
 *  \author Arafat Noureddine
 */
/*************************************************************************/

#ifndef DATA_PARSER_H
#define DATA_PARSER_H

//TANGO
#include <tango.h>
#include "DataStore.h"


namespace XiaDxp_ns
{
//------------------------------------------------------------------
//	class:	DataParser
//------------------------------------------------------------------
class DataParser: public Tango::LogAdapter
{
public:
    ///ctor
    DataParser(Tango::DeviceImpl *dev, DataStore* store);
    ///dtor
    virtual ~DataParser();
    ///Parse entire buffer acquisition
    virtual void parse_buffer(DataBufferContainerPtr map_buffer) = 0;
protected:

    /// Owner Device server object
    Tango::DeviceImpl* m_device;	
    DataStore* m_store;
    std::string m_mode;
    std::string m_board_type;
};

} 

#endif //DATA_PARSER_H
