//=============================================================================
//
// file :        AttrViewMca.cpp
//
// description : 
//
// project :	XiaDxp Project
//
// $Author: noureddine $
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//=============================================================================

#include "AttrViewMca.h"
#include <XiaDxp.h>

namespace XiaDxp_ns
{
//-----------------------------------------------------------------------------
// method :  AttrViewMca::AttrViewMca()
// description : Ctor
//-----------------------------------------------------------------------------
AttrViewMca::AttrViewMca(Tango::DeviceImpl *dev)
:AttrView(dev)
{
	INFO_STREAM << "AttrViewMca::AttrViewMca() - [BEGIN]" << endl;
	m_dyn_preset_value = 0;
	m_dyn_preset_type = 0;
	INFO_STREAM << "AttrViewMca::AttrViewMca() - [END]" << endl;
}

//-----------------------------------------------------------------------------
// method :  AttrViewMca::AttrViewMca()
// description : Dtor
//-----------------------------------------------------------------------------
AttrViewMca::~AttrViewMca()
{
	INFO_STREAM << "AttrViewMca::~AttrViewMca() - [BEGIN]" << endl;
	delete m_dyn_preset_value;
	delete m_dyn_preset_type;
	for(unsigned i = 0;i < m_dyn_channel.size();i++)
	{
		delete m_dyn_channel[i];
	}
	m_dyn_channel.clear();

	for(unsigned i = 0;i < m_dyn_roi.size();++i)
	{
		for(unsigned j = 0;j < m_dyn_roi[i].size();++j)
		{
			delete m_dyn_roi[i][j];
		}
		m_dyn_roi[i].clear();
	}
	m_dyn_roi.clear();
	
	INFO_STREAM << "AttrViewMca::~AttrViewMca() - [END]" << endl;
}

//-----------------------------------------------------------------------------
// method :  AttrViewMca::init()
// description : Create all dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMca::init(yat::SharedPtr<DataStore> data_store)
{
	INFO_STREAM << "AttrViewMca::init() - [BEGIN]" << endl;
	try
	{
		//- remove all previous dyn attributes
		m_dim.dynamic_attributes_manager().remove_attributes();

		//- create common attributes (stats + channels)
		AttrView::init_common_attributes(data_store);

		m_dyn_channel.resize(m_nb_total_channels);
		m_dyn_roi.resize(m_nb_total_channels);
		
		////////////////////////////////////////////////////////////////////////////////////////	
		{
			std::string name = "presetValue";
			INFO_STREAM << "\t- Create Mca dynamic attribute [" << name << "]" << endl;
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_device;

			//- specify the dyn. attr.  name
			dai.tai.name = name;
			//- associate the dyn. attr. with its data 
			m_dyn_preset_value = new DoubleUserData(name);
			dai.set_user_data(m_dyn_preset_value);
			//- describe the dynamic attr we want...
			dai.tai.data_type = Tango::DEV_DOUBLE;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::OPERATOR;
			dai.tai.unit = "sec or counts";
			dai.tai.format = "%6.2f";
			dai.tai.description = "This value is either the number of counts or the time (specified in seconds)\n"
			 "it depends on preset_type parameter.";

			//- instanciate the read callback (called when the dyn. attr. is read)
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMca::read_preset_value_callback);

			//- instanciate the write callback (called when the dyn. attr. is writen)    
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrViewMca::write_preset_value_callback);

			//- add the dyn. attr. to the device
			m_dim.dynamic_attributes_manager().add_attribute(dai);

		}

		////////////////////////////////////////////////////////////////////////////////////////	
		{
			std::string name = "presetType";
			INFO_STREAM << "\t- Create Mca dynamic attribute [" << name << "]" << endl;
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_device;

			//- specify the dyn. attr.  name
			dai.tai.name = name;
			//- associate the dyn. attr. with its data 
			m_dyn_preset_type = new StringUserData(name);
			dai.set_user_data(m_dyn_preset_type);
			//- describe the dynamic attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::OPERATOR;
			dai.tai.unit = " ";
			dai.tai.format = "%s";
			dai.tai.description = "Sets the preset run type:<br>\n"
			 "NONE\n"
			 "FIXED_REAL\n"
			 "FIXED_LIVE\n"
			 "FIXED_EVENTS\n"
			 "FIXED_TRIGGERS";

			//- instanciate the read callback (called when the dyn. attr. is read)
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMca::read_preset_type_callback);

			//- instanciate the write callback (called when the dyn. attr. is writen)    
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &AttrViewMca::write_preset_type_callback);

			//- add the dyn. attr. to the device
			m_dim.dynamic_attributes_manager().add_attribute(dai);
		}

		///////////////////////////////////////////////////////////////////////////////////////////
		for(int i = 0;i < m_nb_total_channels;i++)
		{
			m_dyn_roi[i].resize(m_nb_rois[i]);			
			DEBUG_STREAM << "channel_num = " << i << " : m_nb_rois = " << m_nb_rois[i] << endl;
			for(int j = 0;j < m_nb_rois[i];j++)
			{
				double low = 0, high = 0;
				dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_roi_bounds(i, j, low, high);
				DEBUG_STREAM << "channel_num = " << i << ", roi_num = " << j << ", low = " << low << ", high = " << high << endl;

				//- roi name
				const string name = yat::String::str_format("roi%02d_%02d", i, j + 1);

				INFO_STREAM << "\t- Create Roi dynamic attribute [" << name << "]" << endl;

				yat4tango::DynamicAttributeInfo dai;
				dai.dev = m_device;

				//- specify the dyn. attr.  name
				dai.tai.name = name;
				//- associate the dyn. attr. with its data
				m_dyn_roi[i][j] = new RoiUserData(name, i, j);
				dai.set_user_data(m_dyn_roi[i][j]);
				//- describe the dynamic attr we want...
				dai.tai.data_type = Tango::DEV_DOUBLE;
				dai.tai.data_format = Tango::SCALAR;
				dai.tai.writable = Tango::READ;
				dai.tai.disp_level = Tango::OPERATOR;
				dai.tai.unit = "cts";
				dai.tai.format = "%d";
				dai.tai.description = "";

				//- instanciate the read callback (called when the dyn. attr. is read)    
				dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMca::read_roi_callback);

				//- add the dyn. attr. to the device
				m_dim.dynamic_attributes_manager().add_attribute(dai);
			}

		}

		////////////////////////////////////////////////////////////////////////////////////////

		for(int i = 0;i < m_nb_total_channels;i++)
		{
			std::stringstream ss("");
			std::string name = "channel";
			ss << name << std::setfill('0') << std::setw(2) << i;

			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_device;
			//- specify the dyn. attr.  name
			dai.tai.name = ss.str();
			//- associate the dyn. attr. with its data
			m_dyn_channel[i] = new ChannelUserData(ss.str(), m_nb_bins);
			dai.set_user_data(m_dyn_channel[i]);
			//- describe the dynamic attr we want...
			dai.tai.data_type = Tango::DEV_ULONG;
			dai.tai.data_format = Tango::SPECTRUM;
			dai.tai.writable = Tango::READ;
			 dai.tai.disp_level = Tango::OPERATOR;
			dai.tai.unit = "cts";
			dai.tai.format = "%9d";
			dai.tai.description = "Fluo spectrum for " + ss.str();

			//- instanciate the read callback (called when the dyn. attr. is read)
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &AttrViewMca::read_channel_callback);

			//- add the dyn. attr. to the device
			m_dim.dynamic_attributes_manager().add_attribute(dai);
		}
	}
	catch(Tango::DevFailed& df)
	{
		std::string err("failed to instanciate dynamic attributes  - Tango exception caught - see log attribute for details");
		ERROR_STREAM << err << std::endl;
		ERROR_STREAM << df << std::endl;
		return;
	}


	INFO_STREAM << "AttrViewMca::init() - [END]" << endl;
}


//-----------------------------------------------------------------------------
// method :  AttrViewMca::reinit()
// description : Clear data in the user data object attached to the dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMca::reinit()
{
	INFO_STREAM << "AttrViewMca::reinit() - [BEGIN]" << endl;
	INFO_STREAM << "AttrViewMca::reinit() - [END]" << endl;
}

//-----------------------------------------------------------------------------
//	method:	AttrViewMca::update_data
//	description: update the data of the attributes, this method is fired by the DataStore
//-----------------------------------------------------------------------------
void AttrViewMca::update_data(int ichannel, yat::SharedPtr<DataStore> data_store)
{
	DEBUG_STREAM << "AttrViewMca::update_data() - [BEGIN]" << endl;

	//- update common attributes (stats + channels)	
	AttrView::update_common_attributes(ichannel, data_store);

	//- update channels attributes
	m_dyn_channel[ichannel]->set_value(data_store->get_channel_data(ichannel));

	DEBUG_STREAM << "AttrViewMca::update_data() - [END]" << endl;
}


//-----------------------------------------------------------------------------
// method :         AttrViewMca::read_preset_value_callback()
// description: read callback fct for the presetValue dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMca::read_preset_value_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
	DEBUG_STREAM << "AttrViewMca::read_preset_value_callback()" << endl;//  << cbd.dya->get_name() << endl;

	try
	{
		Tango::DevState state = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_state();
		bool is_device_initialized = dynamic_cast<XiaDxp*> (m_device)->is_device_initialized();
		if((state == Tango::FAULT && is_device_initialized) ||
		state == Tango::INIT ||
		state == Tango::OFF ||
		state == Tango::DISABLE)
		{
			std::string reason = "It's currently not allowed to read attribute presetValue";
			Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
										reason.c_str(),
										"AttrViewMca::read_preset_value_callback()");
		}

		//- be sure the pointer to the dyn. attr. is valid
		if(!cbd.dya)
		{
			THROW_DEVFAILED("INTERNAL_ERROR",
							"unexpected NULL pointer to dynamic attribute",
							"AttrViewMca::read_preset_value_callback");
		}

		DoubleUserData* user_data = cbd.dya->get_user_data<DoubleUserData>();
		//- set the attribute value
		double val = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_preset_value();
		user_data->set_value(val);
		cbd.tga->set_value((Tango::DevDouble*) & user_data->get_value());
	}
	catch(Tango::DevFailed& df)
	{
		ERROR_STREAM << df << endl;
		//- rethrow exception
		Tango::Except::re_throw_exception(df,
										"TANGO_DEVICE_ERROR",
										string(df.errors[0].desc).c_str(),
										"AttrViewMca::read_preset_value_callback()");
	}
}

//-----------------------------------------------------------------------------
// method :         AttrViewMca::write_preset_value_callback()
// description: write callback fct for the presetValue dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMca::write_preset_value_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
	DEBUG_STREAM << "AttrViewMca::write_preset_value_callback()" << endl;//  << cbd.dya->get_name() << endl;
	try
	{
		Tango::DevState state = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_state();
		if(state == Tango::FAULT ||
		state == Tango::INIT ||
		state == Tango::RUNNING ||
		state == Tango::OFF ||
		state == Tango::DISABLE)
		{
			std::string reason = "It's currently not allowed to write attribute presetValue";
			Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
										reason.c_str(),
										"AttrViewMca::write_preset_value_callback()");
		}

		//- be sure the pointer to the dyn. attr. is valid
		if(!cbd.dya)
		{
			THROW_DEVFAILED("INTERNAL_ERROR",
							"unexpected NULL pointer to dynamic attribute",
							"AttrViewMca::write_preset_value_callback");
		}
		Tango::DevDouble previous_memorized = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_preset_value();
		Tango::DevDouble val;
		cbd.tga->get_write_value(val);

		DoubleUserData* user_data = cbd.dya->get_user_data<DoubleUserData>();
		user_data->set_value(val);

		dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->set_preset_value(val);
		//memorize in the device properties
		//memorize only if new value is different from previous value
		if(!yat::fp_is_equal(val, previous_memorized, numeric_limits<double>::epsilon()))
			yat4tango::PropertyHelper::set_property(dynamic_cast<XiaDxp*> (m_device), "__MemorizedPresetValue", val);
		
	}
	catch(Tango::DevFailed& df)
	{
		ERROR_STREAM << df << endl;
		//- rethrow exception
		Tango::Except::re_throw_exception(df,
										"TANGO_DEVICE_ERROR",
										string(df.errors[0].desc).c_str(),
										"AttrViewMca::write_preset_value_callback()");
	}
}

//-----------------------------------------------------------------------------
// method :         AttrViewMca::read_preset_type_callback()
// description: read callback fct for the presetType dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMca::read_preset_type_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
	DEBUG_STREAM << "AttrViewMca::read_preset_type_callback()" << endl;//  << cbd.dya->get_name() << endl;
	try
	{
		Tango::DevState state = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_state();
		bool is_device_initialized = dynamic_cast<XiaDxp*> (m_device)->is_device_initialized();
		if((state == Tango::FAULT && is_device_initialized) ||
		state == Tango::INIT ||
		state == Tango::OFF ||
		state == Tango::DISABLE)
		{
			std::string reason = "It's currently not allowed to read attribute presetType";
			Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
										reason.c_str(),
										"AttrViewMca::read_preset_type_callback()");
		}

		//- be sure the pointer to the dyn. attr. is valid
		if(!cbd.dya)
		{
			THROW_DEVFAILED("INTERNAL_ERROR",
							"unexpected NULL pointer to dynamic attribute",
							"AttrViewMca::read_preset_type_callback()");
		}

		StringUserData* user_data = cbd.dya->get_user_data<StringUserData>();
		//- set the attribute value 
		string current = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_preset_type();
		user_data->set_value(current);
		cbd.tga->set_value((Tango::DevString*)user_data->get_value());
	}
	catch(Tango::DevFailed& df)
	{
		ERROR_STREAM << df << endl;
		//- rethrow exception
		Tango::Except::re_throw_exception(df,
										"TANGO_DEVICE_ERROR",
										string(df.errors[0].desc).c_str(),
										"AttrViewMca::read_preset_type_callback()");
	}
}

//-----------------------------------------------------------------------------
// method :         AttrViewMca::write_preset_type_callback()
// description: write callback fct for the presetType dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMca::write_preset_type_callback(yat4tango::DynamicAttributeWriteCallbackData& cbd)
{
	DEBUG_STREAM << "AttrViewMca::write_preset_type_callback()" << endl;//  << cbd.dya->get_name() << endl;
	try
	{
		Tango::DevState state = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_state();
		if(state == Tango::FAULT ||
		state == Tango::INIT ||
		state == Tango::RUNNING ||
		state == Tango::OFF ||
		state == Tango::DISABLE)
		{
			std::string reason = "It's currently not allowed to write attribute presetType";
			Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
										reason.c_str(),
										"AttrViewMca::write_preset_type_callback()");
		}

		//- be sure the pointer to the dyn. attr. is valid
		if(!cbd.dya)
		{
			THROW_DEVFAILED("INTERNAL_ERROR",
							"unexpected NULL pointer to dynamic attribute",
							"AttrViewMca::write_preset_type_callback");
		}

		string previous_memorized = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_preset_type();
		Tango::DevString val;
		cbd.tga->get_write_value(val);

		string current = val;
		transform(current.begin(), current.end(), current.begin(), ::toupper);
		dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->set_preset_type(current);

		StringUserData* user_data = cbd.dya->get_user_data<StringUserData>();
		user_data->set_value(val);
		//memorize in the device properties
		if(current != previous_memorized)
			yat4tango::PropertyHelper::set_property(dynamic_cast<XiaDxp*> (m_device), "__MemorizedPresetType", current);
	}
	catch(Tango::DevFailed& df)
	{
		ERROR_STREAM << df << endl;
		//- rethrow exception
		Tango::Except::re_throw_exception(df,
										"TANGO_DEVICE_ERROR",
										string(df.errors[0].desc).c_str(),
										"AttrViewMca::write_preset_type_callback()");
	}
}

//-----------------------------------------------------------------------------
// method : AttrViewMca::read_roi_callback()
// description: read callback fct for the rois dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMca::read_roi_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
	try
	{
		Tango::DevState state = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_state();
		bool is_device_initialized = dynamic_cast<XiaDxp*> (m_device)->is_device_initialized();
		if((state == Tango::FAULT && is_device_initialized) ||
		state == Tango::INIT ||
		state == Tango::OFF ||
		state == Tango::DISABLE)
		{
			std::string reason = "It's currently not allowed to read attribute roixx_yy";
			Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
										reason.c_str(),
										"AttrViewMca::read_roi_callback()");
		}

		//- be sure the pointer to the dyn. attr. is valid
		if(!cbd.dya)
		{
			THROW_DEVFAILED("INTERNAL_ERROR",
							"unexpected NULL pointer to dynamic attribute",
							"AttrViewMca::read_roi_callback");
		}

		RoiUserData* user_data = cbd.dya->get_user_data<RoiUserData>();
		//- set the attribute value
		double val = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_roi_data(user_data->get_channel(), user_data->get_roi_num());
		user_data->set_value(val);
		cbd.tga->set_value((Tango::DevDouble*) & user_data->get_value());
	}
	catch(Tango::DevFailed& df)
	{
		ERROR_STREAM << df << endl;
		//- rethrow exception
		Tango::Except::re_throw_exception(df,
										"TANGO_DEVICE_ERROR",
										string(df.errors[0].desc).c_str(),
										"AttrViewMca::read_roi_callback()");
	}
}

//-----------------------------------------------------------------------------
// method : AttrViewMca::read_channel_callback()
// description: read callback fct for the channels spectrum dynamic attrs
//-----------------------------------------------------------------------------
void AttrViewMca::read_channel_callback(yat4tango::DynamicAttributeReadCallbackData& cbd)
{
	DEBUG_STREAM << "AttrViewMca::read_channel_callback() : " << cbd.dya->get_name() << " - [BEGIN]" << endl;

	Tango::DevState state = dynamic_cast<XiaDxp*> (m_device)->get_ctrl()->get_state();
	bool is_device_initialized = dynamic_cast<XiaDxp*> (m_device)->is_device_initialized();
	if((state == Tango::FAULT && is_device_initialized) ||
	state == Tango::INIT ||
	state == Tango::OFF ||
	state == Tango::DISABLE)
	{
		std::string reason = "It's currently not allowed to read attribute : " + cbd.dya->get_name();
		Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
									reason.c_str(),
									"AttrViewMca::read_channel_callback()");
	}

	//- be sure the pointer to the dyn. attr. is valid
	if(!cbd.dya)
	{
		THROW_DEVFAILED("INTERNAL_ERROR",
						"unexpected NULL pointer to dynamic attribute",
						"DynamicInterface::read_callback");
	}

	try
	{
		ChannelUserData* channel_data = cbd.dya->get_user_data<ChannelUserData>();
		//- set the attribute value
		cbd.tga->set_value(const_cast<Tango::DevULong*> (&channel_data->get_value()[0]), channel_data->get_value().size());
	}
	catch(Tango::DevFailed& df)
	{
		ERROR_STREAM << df << endl;
		//- rethrow exception
		Tango::Except::re_throw_exception(df,
										"TANGO_DEVICE_ERROR",
										string(df.errors[0].desc).c_str(),
										"AttrViewMca::read_channel_callback");
	}
	DEBUG_STREAM << "AttrViewMca::read_channel_callback() - [END]" << endl;
}

}// namespace
