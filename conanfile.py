from conan import ConanFile

class XiaDxpRecipe(ConanFile):
    name = "xiadxp"
    executable = "ds_XiaDxp"
    version = "3.4.0"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Arafat Nourredine"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/acquisition/1d/xiadxp.git"
    description = "XiaDxp device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*", "sdk/**"
    
    def requirements(self):
        self.requires("nexuscpp/[>=3 <4]@soleil/stable")
        self.requires("hdf5/[>=1.0]@soleil/stable")
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
